import { createSlice } from "@reduxjs/toolkit";
import { notify } from "../../components/Constants";

const initialState = {
    isLoading: false,
    drivers: [],
    driverspage: 1,
    driversloading: false,
};

export const driverSlice = createSlice({
    name: 'driver',
    initialState,
    reducers: {
        adddriverInit: (state) => {
            state.isLoading = true;
        },
        adddriverSuccess: (state, action) => {
            state.isLoading = false;
            let newdata = { ...action.payload.data };
            newdata.id = action.payload.id;
            state.drivers.unshift(newdata);
            notify('snackbar', {
                params: {
                    title: action.payload.message,
                },
            });
        },
        adddriverFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload,
                },
            });
        },
        fetchdriverInit: (state) => {
            state.isLoading = true;
        },
        fetchdrivermore: (state) => {
            state.isLoading = false;
        },
        fectchdriverSuccess: (state, action) => {
            state.isLoading = false;
            state.drivers.push(...action.payload.data);
            state.driverspage += 1;
            state.driversloading = action.payload.loadmore;
        },
        fectchdriverFailure: (state) => {
            state.isLoading = false;
        },
        updatedriverInit: (state) => {
            state.isLoading = true;
        },
        updatedriverSuccess: (state, action) => {
            const existItem = state.drivers.find((item) => item.id === action.payload.id);
            existItem.address = action.payload.data.address;
            existItem.contact_no = action.payload.data.contact_no;
            existItem.country_code = action.payload.data.country_code;
            existItem.email = action.payload.data.email;
            existItem.license_doc = action.payload.data.license_doc;
            existItem.license_no = action.payload.data.license_no;
            existItem.license_validity = action.payload.data.license_validity;
            existItem.name = action.payload.data.name;
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload.message,
                },
            });
        },
        updatedriverFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload,
                },
            });
        },
    }
});

export const { adddriverInit, adddriverSuccess, adddriverFailure, snackHide, fetchdriverInit, fetchdrivermore, fectchdriverSuccess, fectchdriverFailure, updatedriverInit, updatedriverSuccess, updatedriverFailure } = driverSlice.actions;

export default driverSlice.reducer;

export const driverlist = (state) => state.driver.drivers;
