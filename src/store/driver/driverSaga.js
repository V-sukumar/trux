import { all, call, put, takeLatest } from 'redux-saga/effects';
import { adddriverSuccess, adddriverFailure, fectchdriverSuccess, fectchdriverFailure, updatedriverSuccess, updatedriverFailure } from './driverSlice';
import { addDriverApi, driverlistApi, updateDriverApi } from '../../components/api';
import * as RootNavigation from "../../components/RootNavigation";

function* addDriver(action) {
    const { payload } = action;
    try {
        const { data } = yield call(addDriverApi, payload);
        if (data.status === "success") {
            yield put(adddriverSuccess({ message: data.message, data: payload.newdata, id: data.id }));
            RootNavigation.back();
        } else {
            yield put(adddriverFailure(data.message));
            RootNavigation.back();
        }
    } catch (e) {
        yield put(adddriverFailure("Server error try again."));
        RootNavigation.back();
    }
}

function* updateDriver(action) {
    const { payload } = action;
    try {
        const { data } = yield call(updateDriverApi, payload);
        if (data.status === "success") {
            yield put(updatedriverSuccess({ message: data.message, data: payload.newdata, id: payload.id }));
            RootNavigation.back();
        } else {
            yield put(updatedriverFailure(data.message));
            RootNavigation.back();
        }
    } catch (e) {
        yield put(updatedriverFailure("Server error try again."));
        RootNavigation.back();
    }
}

function* fetchdriver(action) {
    const { payload } = action;
    try {
        const { data } = yield call(driverlistApi, payload);
        if (data.status === "success") {
            let loadmore = true;
            if (data.data.length < 10) {
                loadmore = false;
            }
            yield put(fectchdriverSuccess({ data: data.data, loadmore }));
        } else {
            yield put(fectchdriverFailure(data.message));
        }
    } catch (e) {
        yield put(fectchdriverFailure("Server error try again."));
    }
}

function* driverSaga() {
    yield all([
        takeLatest('driver/adddriverInit', addDriver),
        takeLatest('driver/updatedriverInit', updateDriver),
        takeLatest('driver/fetchdriverInit', fetchdriver),
        takeLatest('driver/fetchdrivermore', fetchdriver),
    ]);
}

export default driverSaga;
