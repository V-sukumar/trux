import { call, put, takeLatest } from 'redux-saga/effects';
import { custregisterSuccess, custregisterFailure } from './customersignupSlice';
import { customerRegisterApi } from "../../components/api";
import * as RootNavigation from "../../components/RootNavigation";

function* cusRegister(action) {
    const { payload } = action;
    try {
        const { data } = yield call(customerRegisterApi, payload.formdata);
        if (data.status === "success") {
            yield put(custregisterSuccess());
            RootNavigation.navigate("Otp", { email: payload.email });
        } else {
            yield put(custregisterFailure(data.message));
        }
    } catch (e) {
        yield put(custregisterFailure("Server error try again"));
    }
}

function* customersignupSaga() {
    yield takeLatest('customersignup/custregisterInit', cusRegister);
}

export default customersignupSaga;
