import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    error: "",
};

export const customersignupSlice = createSlice({
    name: 'customersignup',
    initialState,
    reducers: {
        custregisterInit: (state) => {
            state.isLoading = true;
            state.error = "";
        },
        custregisterSuccess: (state) => {
            state.isLoading = false;
        },
        custregisterFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    }
});

export const { custregisterInit, custregisterSuccess, custregisterFailure } = customersignupSlice.actions;

export default customersignupSlice.reducer;
