import { all, call, spawn } from 'redux-saga/effects';
import authSaga from './auth/authSaga';
import customersignupSaga from './customersignup/customersignupSaga';
import customerprofileSaga from './customerprofile/customerprofileSaga';
import transportersignupSaga from './transportersignup/transportersignupSaga';
import transporterprofileSaga from './transporterprofile/transporterprofileSaga';
import truckSaga from './truck/truckSaga';
import driverSaga from './driver/driverSaga';
import verifyotpSaga from './verifyotp/verifyotpSaga';
import forgotpasswordSaga from './forgotpassword/forgotpasswordSaga';
import trucktypeSaga from './trucktype/trucktypeSaga';
import masterSaga from './master/masterSaga';
import supportSaga from './support/supportSaga';

export default function* rootSaga() {
    const sagas = [
        authSaga,
        customersignupSaga,
        customerprofileSaga,
        transportersignupSaga,
        transporterprofileSaga,
        truckSaga,
        driverSaga,
        verifyotpSaga,
        forgotpasswordSaga,
        trucktypeSaga,
        masterSaga,
        supportSaga,
    ];

    yield all(sagas.map(saga =>
        spawn(function* () {
            while (true) {
                try {
                    yield call(saga);
                    break;
                } catch (e) {
                }
            }
        }))
    );
}