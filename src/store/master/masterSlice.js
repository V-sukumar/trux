import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    privacypolicy: "",
    termsandconditions: "",
    faqs: [],
    error: "",
};

export const masterSlice = createSlice({
    name: 'master',
    initialState,
    reducers: {
        getFaq: (state) => {
            state.isLoading = true;
        },
        setFaq: (state, action) => {
            state.isLoading = false;
            state.faqs = action.payload;
        },
        getPrivacy: (state) => {
            state.isLoading = true;
        },
        setPrivacy: (state, action) => {
            state.isLoading = false;
            state.privacypolicy = action.payload;
        },
        getTerms: (state) => {
            state.isLoading = true;
        },
        setTerms: (state, action) => {
            state.isLoading = false;
            state.termsandconditions = action.payload;
        },
        mastererror: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    },
});

export const { getPrivacy, setPrivacy, getTerms, setTerms, getFaq, setFaq, mastererror } = masterSlice.actions;

export default masterSlice.reducer;
