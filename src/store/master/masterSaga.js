import { all, call, put, takeLatest } from 'redux-saga/effects';
import { setPrivacy, setTerms, setFaq, mastererror } from './masterSlice';
import { privacypolicyApi, termsconditionsApi, faqApi } from "../../components/api";

function* fetchTerms() {
    try {
        const { data } = yield call(termsconditionsApi);
        yield put(setTerms(data.message));

    } catch (e) {
        yield put(mastererror("Server error try again"));
    }
}

function* fetchPrivacy() {
    try {
        const { data } = yield call(privacypolicyApi);
        yield put(setPrivacy(data.message));

    } catch (e) {
        yield put(mastererror("Server error try again"));
    }
}

function* fetchFaqs() {
    try {
        const { data } = yield call(faqApi);
        yield put(setFaq(data));

    } catch (e) {
        yield put(mastererror("Server error try again"));
    }
}

function* masterSaga() {
    yield all([
        takeLatest('master/getPrivacy', fetchPrivacy),
        takeLatest('master/getTerms', fetchTerms),
        takeLatest('master/getFaq', fetchFaqs),
    ]);
}

export default masterSaga;
