import { all, call, put, takeLatest } from 'redux-saga/effects';
import { profileFailure, profileSuccess, updateSuccess, updateFailure } from './customerprofileSlice';
import { customerprofileApi, custupdateprofileApi } from "../../components/api";

function* fetchprofile(action) {
    const { payload } = action;
    try {
        const { data } = yield call(customerprofileApi, payload);
        yield put(profileSuccess(data));

    } catch (e) {
        yield put(profileFailure("Server error try again"));
    }
}

function* updaterofile(action) {
    const { payload } = action;
    try {
        const { data } = yield call(custupdateprofileApi, payload);
        if (data.status === "success") {
            yield put(updateSuccess({ message: data.message, data: payload.newdata }));
        } else {
            yield put(updateFailure(data.message));
        }
    } catch (e) {
        yield put(updateFailure("Server error try again"));
    }
}

function* customerprofileSaga() {
    yield all([
        takeLatest('customerprofile/cusprofileFetch', fetchprofile),
        takeLatest('customerprofile/updateInit', updaterofile),
    ]);
}

export default customerprofileSaga;
