import { createSlice } from "@reduxjs/toolkit";
import { notify } from "../../components/Constants";

const initialState = {
    profileInfo: {},
    isLoading: false,
    error: "",
};

export const customerprofileSlice = createSlice({
    name: 'customerprofile',
    initialState,
    reducers: {
        cusprofileFetch: (state) => {
            state.isLoading = true;
        },
        profileSuccess: (state, action) => {
            state.isLoading = false;
            state.profileInfo = action.payload;
        },
        profileFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
        updateInit: (state) => {
            state.isLoading = true;
        },
        updateSuccess: (state, action) => {
            const profiledata = state.profileInfo;
            const { company_name, email, contact_no, name, company_address, country_code, trade_license_no, trade_license_doc } = action.payload.data;
            profiledata.company_name = company_name;
            profiledata.email = email;
            profiledata.contact_no = contact_no;
            profiledata.name = name;
            profiledata.company_address = company_address;
            profiledata.country_code = country_code;
            profiledata.trade_license_no = trade_license_no;
            profiledata.trade_license_doc = trade_license_doc;
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload.message,
                },
            });
        },
        updateFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload,
                },
            });
        },
    },
});

export const { cusprofileFetch, profileSuccess, profileFailure, updateInit, updateSuccess, updateFailure } = customerprofileSlice.actions;

export default customerprofileSlice.reducer;
