import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    error: "",
};

export const transportersignupSlice = createSlice({
    name: 'transportersignup',
    initialState,
    reducers: {
        transregisterInit: (state) => {
            state.isLoading = true;
            state.error = "";
        },
        transregisterSuccess: (state) => {
            state.isLoading = false;
        },
        transregisterFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    },
});

export const { transregisterInit, transregisterSuccess, transregisterFailure } = transportersignupSlice.actions;

export default transportersignupSlice.reducer;
