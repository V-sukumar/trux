import { call, put, takeLatest } from 'redux-saga/effects';
import { transregisterSuccess, transregisterFailure } from './transportersignupSlice';
import { transporterRegisterApi } from "../../components/api";
import * as RootNavigation from "../../components/RootNavigation";

function* transRegister(action) {
    const { payload } = action;
    try {
        const { data } = yield call(transporterRegisterApi, payload.formdata);
        if (data.status === "success") {
            yield put(transregisterSuccess());
            RootNavigation.navigate("Otp", { email: payload.email });
        } else {
            yield put(transregisterFailure(data.message));
        }
    } catch (e) {
        yield put(transregisterFailure("Server error try again"));
    }
}

function* transportersignupSaga() {
    yield takeLatest('transportersignup/transregisterInit', transRegister);
}

export default transportersignupSaga;
