import { createSlice } from "@reduxjs/toolkit";
import { notify } from "../../components/Constants";

const initialState = {
    isLoading: false,
    trucks: [],
    truckspage: 1,
    trucksloading: false,
};

export const truckSlice = createSlice({
    name: 'truck',
    initialState,
    reducers: {
        addtruckInit: (state) => {
            state.isLoading = true;
        },
        addtruckSuccess: (state, action) => {
            state.isLoading = false;
            let newdata = {...action.payload.data};
            newdata.id = action.payload.id;
            state.trucks.unshift(newdata);
            notify('snackbar', {
                params: {
                  title: action.payload.message,
                },
            });
        },
        addtruckFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                  title: action.payload,
                },
            });
        },
        fetchtruckInit: (state) => {
            state.isLoading = true;
        },
        fetchtruckmore: (state) => {
            state.isLoading = false;
        },
        fectchtruckSuccess: (state, action) => {
            state.isLoading = false;
            state.trucks.push(...action.payload.data);
            state.truckspage += 1;
            state.trucksloading = action.payload.loadmore;
        },
        fectchtruckFailure: (state) => {
            state.isLoading = false;
        },
        updatetruckInit: (state) => {
            state.isLoading = true;
        },
        updatetruckSuccess: (state, action) => {
            const existItem = state.trucks.find((item) => item.id === action.payload.id);
            existItem.cargo_insurance = action.payload.data.cargo_insurance;
            existItem.compilances = action.payload.data.compilances;
            existItem.insurance = action.payload.data.insurance;
            existItem.insurance_validity = action.payload.data.insurance_validity;
            existItem.reg_card = action.payload.data.reg_card;
            existItem.reg_expire_date = action.payload.data.reg_expire_date;
            existItem.truck_no = action.payload.data.truck_no;
            existItem.truck_photo = action.payload.data.truck_photo;
            existItem.truck_type = action.payload.data.truck_type;
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload.message,
                },
            });
        },
        updatetruckFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                    title: action.payload,
                },
            });
        },
    }
});

export const { addtruckInit, addtruckSuccess, addtruckFailure, fetchtruckInit, fetchtruckmore, fectchtruckSuccess, fectchtruckFailure, updatetruckInit, updatetruckSuccess, updatetruckFailure } = truckSlice.actions;

export default truckSlice.reducer;

export const trucklist = (state) => state.truck.trucks;
