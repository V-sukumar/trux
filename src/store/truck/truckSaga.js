import { all, call, put, takeLatest } from 'redux-saga/effects';
import { addtruckSuccess, addtruckFailure, fectchtruckSuccess, fectchtruckFailure, updatetruckSuccess, updatetruckFailure } from './truckSlice';
import { addtruckApi, trucklistApi, updateTruckApi } from '../../components/api';
import * as RootNavigation from "../../components/RootNavigation";

function* addTruck(action) {
    const { payload } = action;
    try {
        const { data } = yield call(addtruckApi, payload);
        if (data.status === "success") {
            yield put(addtruckSuccess({ message: data.message, data: payload.newdata, id: data.id }));
            RootNavigation.back();
        } else {
            yield put(addtruckFailure(data.message));
            RootNavigation.back();
        }
    } catch (e) {
        yield put(addtruckFailure("Server error try again."));
        RootNavigation.back();
    }
}

function* updateTruck(action) {
    const { payload } = action;
    try {
        const { data } = yield call(updateTruckApi, payload);
        if (data.status === "success") {
            yield put(updatetruckSuccess({ message: data.message, data: payload.newdata, id: payload.id }));
            RootNavigation.back();
        } else {
            yield put(updatetruckFailure(data.message));
            RootNavigation.back();
        }
    } catch (e) {
        yield put(updatetruckFailure("Server error try again."));
        RootNavigation.back();
    }
}

function* fetchtruck(action) {
    const { payload } = action;
    try {
        const { data } = yield call(trucklistApi, payload);
        if (data.status === "success") {
            let loadmore = true;
            if (data.data.length < 10) {
                loadmore = false;
            }
            yield put(fectchtruckSuccess({ data: data.data, loadmore }));
        } else {
            yield put(fectchtruckFailure(data.message));
        }
    } catch (e) {
        yield put(fectchtruckFailure("Server error try again."));
    }
}

function* truckSaga() {
    yield all([
        takeLatest('truck/addtruckInit', addTruck),
        takeLatest('truck/updatetruckInit', updateTruck),
        takeLatest('truck/fetchtruckInit', fetchtruck),
        takeLatest('truck/fetchtruckmore', fetchtruck),
    ]);
}

export default truckSaga;
