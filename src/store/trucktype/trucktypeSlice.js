import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    trucktypes: [],
    isLoading: false,
    error: "",
};

export const trucktypeSlice = createSlice({
    name: 'trucktype',
    initialState,
    reducers: {
        trucktypeFetch: (state) => {
            state.isLoading = true;
        },
        trucktypeSuccess: (state, action) => {
            state.isLoading = false;
            state.trucktypes = action.payload;
        },
        trucktypeFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    },
});

export const { trucktypeFetch, trucktypeSuccess, trucktypeFailure } = trucktypeSlice.actions;

export default trucktypeSlice.reducer;

export const trucktypelist = (state) => state.trucktype.trucktypes;
