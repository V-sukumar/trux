import { call, put, takeLatest } from 'redux-saga/effects';
import { trucktypeSuccess, trucktypeFailure } from './trucktypeSlice';
import { trucktypedApi } from "../../components/api";

function* getTrucktype() {
    try {
        const { data } = yield call(trucktypedApi);
        yield put(trucktypeSuccess(data));
    } catch (e) {
        yield put(trucktypeFailure("Server error try again"));
    }
}

function* trucktypeSaga() {
    yield takeLatest('trucktype/trucktypeFetch', getTrucktype);
}

export default trucktypeSaga;
