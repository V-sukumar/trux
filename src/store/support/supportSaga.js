import { call, all, put, takeLatest } from 'redux-saga/effects';
import { supportSuccess, supportFailure } from './supportSlice';
import { supportApi } from "../../components/api";

function* submitsupport(action) {
    const { formdata, resetForm } = action.payload;
    try {
        const { data } = yield call(supportApi, formdata);
        if (data.status === "success") {
            yield all([
                resetForm({ values: "" }),
                put(supportSuccess(data.message)),
            ]);
        } else {
            yield put(supportFailure(data.message));
        }
    } catch (e) {
        yield put(supportFailure("Server error try again"));
    }
}

function* supportSaga() {
    yield takeLatest('support/supportInit', submitsupport);
}

export default supportSaga;
