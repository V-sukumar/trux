import { createSlice } from "@reduxjs/toolkit";
import { notify } from "../../components/Constants";

const initialState = {
    isLoading: false,
};

export const supportSlice = createSlice({
    name: 'support',
    initialState,
    reducers: {
        supportInit: (state) => {
            state.isLoading = true;
        },
        supportSuccess: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                  title: action.payload,
                },
            });
        },
        supportFailure: (state, action) => {
            state.isLoading = false;
            notify('snackbar', {
                params: {
                  title: action.payload,
                },
            });
        },
    },
});

export const { supportInit, supportSuccess, supportFailure } = supportSlice.actions;

export default supportSlice.reducer;
