import { all, call, put, takeLatest } from 'redux-saga/effects';
import { profileFailure, profileSuccess, updateSuccess, updateFailure } from './transporterprofileSlice';
import { transporterprofileApi, transupdateprofileApi } from "../../components/api";

function* fetchprofile(action) {
    const { payload } = action;
    try {
        const { data } = yield call(transporterprofileApi, payload);
        yield put(profileSuccess(data));

    } catch (e) {
        yield put(profileFailure("Server error try again"));
    }
}

function* updaterofile(action) {
    const { payload } = action;
    try {
        const { data } = yield call(transupdateprofileApi, payload);
        if (data.status === "success") {
            yield put(updateSuccess({ message: data.message, data: payload.newdata }));
        } else {
            yield put(updateFailure(data.message));
        }
    } catch (e) {
        yield put(updateFailure("Server error try again"));
    }
}

function* transporterprofileSaga() {
    yield all([
        takeLatest('transporterprofile/profileFetch', fetchprofile),
        takeLatest('transporterprofile/updateInit', updaterofile),
    ]);
}

export default transporterprofileSaga;
