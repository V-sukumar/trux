import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    error: "",
};

export const verifyotpSlice = createSlice({
    name: 'verifyotp',
    initialState,
    reducers: {
        otpverify: (state) => {
            state.isLoading = true;
            state.error = "";
        },
        otpverified: (state) => {
            state.isLoading = false;
        },
        otpfailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    }
});

export const { otpverify, otpverified, otpfailure } = verifyotpSlice.actions;

export default verifyotpSlice.reducer;
