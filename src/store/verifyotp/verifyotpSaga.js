import { call, put, takeLatest } from 'redux-saga/effects';
import { otpverified, otpfailure } from './verifyotpSlice';
import { verifyOtpApi } from "../../components/api";
import * as RootNavigation from "../../components/RootNavigation";

function* otpVerification(action) {
    const { payload } = action;
    try {
        const { data } = yield call(verifyOtpApi, payload);
        if (data.status === "success") {
            yield put(otpverified());
            RootNavigation.navigate("Thankyou");
        } else {
            yield put(otpfailure(data.message));
        }
    } catch (e) {
        yield put(otpfailure("Server error try again"));
    }
}

function* verifyotpSaga() {
    yield takeLatest('verifyotp/otpverify', otpVerification);
}

export default verifyotpSaga;
