import { call, all, put, takeLatest } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { authSuccess, authFailure } from './authSlice';
import { loginApi } from "../../components/api";

function* login(action) {
    const { payload } = action;
    try {
        const { data } = yield call(loginApi, payload);
        if (data.status === "success") {
            const userinfo = ["@userinfo", JSON.stringify(data.user_info)];
            const token = ["@token", JSON.stringify(data.token)];
            yield all([
                AsyncStorage.multiSet([userinfo, token]),
                put(authSuccess(data.user_info)),
            ]);
        } else {
            yield put(authFailure(data.message));
        }
    } catch (e) {
        yield put(authFailure("Server error try again"));
    }
}

function* authSaga() {
    yield takeLatest('auth/authInit', login);
}

export default authSaga;
