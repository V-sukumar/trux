import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    userInfo: null,
    isLoading: false,
    error: "",
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        authInit: (state) => {
            state.isLoading = true;
            state.error = "";
        },
        authSuccess: (state, action) => {
            state.isLoading = false;
            state.userInfo = action.payload;
        },
        authFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
        logout: (state) => {
            state.userInfo = null;
        },
        authUpdate: (state, action) => {
            state.userInfo = action.payload;
        },
    },
});

export const { authInit, authSuccess, authFailure, logout, authUpdate } = authSlice.actions;

export default authSlice.reducer;
