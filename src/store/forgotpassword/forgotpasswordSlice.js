import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLoading: false,
    isUpdated: false,
    msg: "",
    error: "",
};

export const forgotpasswordSlice = createSlice({
    name: 'forgotpassword',
    initialState,
    reducers: {
        forgotInit: (state) => {
            state.isLoading = true;
            state.error = "";
        },
        forgotSuccess: (state, action) => {
            state.isLoading = false;
            state.isUpdated = true;
            state.msg = action.payload;
        },
        forgotFailure: (state, action) => {
            state.isLoading = false;
            state.error = action.payload;
        },
    }
});

export const { forgotInit, forgotSuccess, forgotFailure } = forgotpasswordSlice.actions;

export default forgotpasswordSlice.reducer;
