import { call, put, takeLatest } from 'redux-saga/effects';
import { forgotSuccess, forgotFailure } from './forgotpasswordSlice';
import { forgotpwdApi } from "../../components/api";
import * as RootNavigation from "../../components/RootNavigation";

function* initForgotpwd(action) {
    const { payload } = action;
    try {
        const { data } = yield call(forgotpwdApi, payload);
        if (data.status === "success") {
            yield put(forgotSuccess("Otp sent mail"));
            RootNavigation.navigate("Otp", { email: payload.email, screen: "ChangePassword" });
        } else {
            yield put(forgotFailure(data.message));
        }
    } catch (e) {
        yield put(forgotFailure("Server error try again"));
    }
}

function* forgotpasswordSaga() {
    yield takeLatest('forgotpassword/forgotInit', initForgotpwd);
}

export default forgotpasswordSaga;
