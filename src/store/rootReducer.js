import { combineReducers } from "redux";
import authSlice from "./auth/authSlice";
import verifyotpSlice from "./verifyotp/verifyotpSlice";
import truckSlice from "./truck/truckSlice";
import customersignupSlice from "./customersignup/customersignupSlice";
import customerprofileSlice from "./customerprofile/customerprofileSlice";
import transportersignupSlice from "./transportersignup/transportersignupSlice";
import transporterprofileSlice from "./transporterprofile/transporterprofileSlice";
import driverSlice from "./driver/driverSlice";
import forgotpasswordSlice from "./forgotpassword/forgotpasswordSlice";
import trucktypeSlice from "./trucktype/trucktypeSlice";
import masterSlice from "./master/masterSlice";
import supportSlice from "./support/supportSlice";

const allReducers = combineReducers({
    auth: authSlice,
    truck: truckSlice,
    driver: driverSlice,
    verifyotp: verifyotpSlice,
    customersignup: customersignupSlice,
    customerprofile: customerprofileSlice,
    transportersignup: transportersignupSlice,
    transporterprofile: transporterprofileSlice,
    forgotpassword: forgotpasswordSlice,
    trucktype: trucktypeSlice,
    master: masterSlice,
    support: supportSlice,
});

export default rootReducer = (state, action) => {
    if (action.type === "auth/logout") {
        return allReducers(undefined, action);
    }
    return allReducers(state, action);
}
