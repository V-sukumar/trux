import React, { useState } from 'react';
import { TouchableHighlight, ScrollView, Text, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import Modal from "react-native-modal";
import { bahrain, kuwait, oman, qatar, saudi, uae } from "../../img/FlagIcon";
import { downarrow } from "../../img/AuthIcon";
import { closeicon } from "../../img/AppIcon";
import { styles } from './style';

const countries = [
    { flag: bahrain, code: "973", country: "Bahrain" },
    { flag: kuwait, code: "965", country: "Kuwait" },
    { flag: oman, code: "968", country: "Oman" },
    { flag: qatar, code: "974", country: "Qatar" },
    { flag: saudi, code: "966", country: "Saudi Arabia" },
    { flag: uae, code: "971", country: "United Arab Emirates" },
];

function CountryCode(props) {
    const { data, value, setFieldValue } = props;
    const [open, setOpen] = useState(false);
    const [flag, setFlag] = useState(uae);

    function openModal() {
        setOpen(true);
    }

    function setCode(code, cflag) {
        setFieldValue(data, code);
        setFlag(cflag);
        closeModal();
    }

    function closeModal() {
        setOpen(false);
    }

    return (
        <>
            <Modal
                isVisible={open}
                animationIn="fadeIn"
                animationOut="fadeOut"
                hideModalContentWhileAnimating={true}
                backdropTransitionOutTiming={0}
                backdropColor="#000"
                backdropOpacity={0.5}
                style={styles.modal}
            >
                <View style={styles.modalInner}>
                    <View style={styles.modalcont}>
                        <TouchableHighlight
                            style={styles.closeico}
                            onPress={closeModal}
                            underlayColor="transparent"
                        >
                            <SvgXml xml={closeicon} width="10" height="10" />
                        </TouchableHighlight>
                    </View>
                    <ScrollView
                        keyboardShouldPersistTaps="always"
                        contentContainerStyle={styles.scrollCont}
                    >
                        {countries.map((item, i) =>
                            <TouchableHighlight
                                key={i}
                                onPress={() => setCode(item.code, item.flag)}
                                style={styles.countrybtn}
                                underlayColor="transparent"
                            >
                                <>
                                    <View style={styles.popupflags}>
                                        <SvgXml xml={item.flag} width="30" height="24" />
                                        <Text style={styles.mobilecode}>{item.code}</Text>
                                    </View>
                                    <Text style={styles.mobilecode}>{item.country}</Text>
                                </>
                            </TouchableHighlight>
                        )}
                    </ScrollView>
                </View>
            </Modal>
            <TouchableHighlight
                style={[styles.fieldicon, styles.fieldmobile]}
                onPress={openModal}
                underlayColor="transparent"
            >
                <>
                    <View style={styles.fieldmobileicons}>
                        <View style={styles.flagdiv}>
                            <SvgXml xml={flag} width="24" height="24" />
                        </View>
                        <Text style={styles.mobilecode}>+{value}</Text>
                        <SvgXml xml={downarrow} width="15" height="15" />
                    </View>
                    <View style={styles.separator} />
                </>
            </TouchableHighlight>
        </>
    );
}

export default CountryCode;
