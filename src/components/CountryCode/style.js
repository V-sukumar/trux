import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    modal: {
        alignItems: "center",
        justifyContent: "center",
    },
    modalInner: {
        backgroundColor: '#fff',
        width: ScreenWidth * .8,
        height: 310,
        borderRadius: 4,
        overflow: 'hidden',
    },
    modalcont: {
        padding: 10
    },
    closeico: {
        position: 'absolute',
        zIndex: 1,
        right: 6,
        top: 6,
        height: 22,
        width: 22,
        justifyContent: 'center',
        alignItems: 'center'
    },
    scrollCont: {
        padding: 10,
    },
    fieldicon: {
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 6,
        height: 50,
        width: 50,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    flagdiv: {
        overflow: "hidden",
    },
    fieldmobile: {
        left: 34,
        flexDirection: "row"
    },
    fieldmobileicons: {
        flexDirection: "row",
        alignItems: "center",
    },
    mobilecode: {
        fontSize: 14,
        color: "#03353C",
        fontFamily: "Poppins-Regular",
        paddingHorizontal: 6,
    },
    separator: {
        width: 1,
        height: 24,
        backgroundColor: "#9EA6A7",
        marginLeft: 10,
    },
    popupflags: {
        flexDirection: "row",
    },
    countrybtn: {
        flexDirection: "row",
        padding: 10,
        alignItems: "center",
    },
});
