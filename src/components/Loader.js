import React from "react";
import { ActivityIndicator, StyleSheet } from 'react-native';
import Modal from "react-native-modal";

function Loader(props) {
    return (
        <Modal
            isVisible={props.isVisible}
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            backdropTransitionOutTiming={0}
            style={styles.modal}
            backdropColor={props.backdropColor ? props.backdropColor : "#ffffff"}
            backdropOpacity={0.5}
        >
            <ActivityIndicator size="large" color="#094B54" />
        </Modal>
    )
}

const styles = StyleSheet.create({
    modal: {
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Loader;
