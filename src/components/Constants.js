import { Dimensions, View, Text, StyleSheet } from "react-native";
import { createNotifications, AnimationBuilder } from 'react-native-notificated';

export const FontScale = Dimensions.get('window').fontScale;
export const ScreenWidth = Dimensions.get('window').width;
export const ScreenHeight = Dimensions.get('window').height;
export const MAX_FILE_SIZE = 20 * 1024 * 1024;
export const SnackBar = ({ title }) => {
    return (
        <View style={styles.snackouter}>
            <View style={styles.snackinner}>
                <Text style={styles.snacktxt}>{title}</Text>
            </View>
        </View>
    );
};
export const MoveDown = new AnimationBuilder({
    animationConfigIn: {
        type: 'timing',
        config: {
            duration: 500,
        },
    },
    transitionInStyles: (progress) => {
        'worklet'
        return {
            opacity: progress.value,
        }
    },
    transitionOutStyles: (progress) => {
        'worklet'
        return {
            opacity: progress.value,
        }
    },
});
export const { NotificationsProvider, notify } = createNotifications({
    variants: {
        snackbar: {
            component: SnackBar,
            config: {
                duration: 2000,
                notificationPosition: 'bottom',
                animationConfig: MoveDown,
                notificationWidth: ScreenWidth,
            },
        },
    },
});
const styles = StyleSheet.create({
    snackouter: {
        alignItems: "center",
    },
    snackinner: {
        backgroundColor: 'rgba(0, 0, 0, 0.9)',
        borderRadius: 30,
        minHeight: 50,
        alignItems: "center",
        justifyContent: "center",
        width: 300,
    },
    snacktxt: {
        color: "#fff",
        fontSize: 14,
        fontFamily: "Poppins-Regular",
    },
});
