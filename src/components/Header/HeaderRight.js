import React from 'react';
import { View, TouchableHighlight } from "react-native";
import { SvgXml } from 'react-native-svg';
import { search, notification } from '../../img/AppIcon';
import { styles } from './style';

function HeaderRight() {
    return (
        <View style={styles.rightontainer}>
            <TouchableHighlight style={[styles.menubtn, styles.rightbtn]}>
                <SvgXml xml={search} width="22" height="22" />
            </TouchableHighlight>
            <TouchableHighlight style={[styles.menubtn, styles.rightbtn]}>
                <SvgXml xml={notification} width="22" height="22" />
            </TouchableHighlight>
        </View>
    );
}

export default HeaderRight;
