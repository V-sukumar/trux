import React from 'react';
import { View, TouchableHighlight } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { SvgXml } from 'react-native-svg';
import { menu } from '../../img/AppIcon';
import { styles } from './style';

function HeaderLeft() {
    const navigation = useNavigation();

    function openMenu() { 
        navigation.openDrawer() 
    }

    return (
        <View style={styles.leftcontainer}>
            <TouchableHighlight underlayColor="#094B54" style={styles.menubtn} onPress={openMenu}>
                <SvgXml xml={menu} width="22" height="22" />
            </TouchableHighlight>
        </View>
    );
}

export default HeaderLeft;
