import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    leftcontainer: {
        marginStart: 10,
    },
    rightontainer: {
        flexDirection: "row",
        marginEnd: 10,
    },
    menubtn:{
        height: 44,
        width: 44,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 100,
    },
    rightbtn: {
        marginLeft: 20
    },
});
