import React, { useState, useEffect, useRef } from 'react';
import { TextInput, TouchableHighlight } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle, withTiming, interpolate } from 'react-native-reanimated';
import { styles } from './style';


function AddressInput(props) {
    const inputRef = useRef();
    const { style, value, leftDistance, label, onChangeText, onBlur, validationStyle, keyboardType, secureTextEntry } = props;
    const animateIsFocused = useSharedValue(value === '' ? 0 : 1);
    const [isFocused, setIsFocused] = useState(false);

    useEffect(() => {
        animateIsFocused.value = withTiming((isFocused || value !== '') ? 1 : 0, { duration: 150 });
    }, [isFocused, value]);

    function focusinput() {
        inputRef.current.focus();
    }

    function handleFocus(e) {
        setIsFocused(true);
    }

    function handleBlur(e) {
        setIsFocused(false);
        onBlur(e);
    }

    const labelStyle = useAnimatedStyle(() => {
        return {
            left: (leftDistance === undefined) ? 10 : leftDistance,
            top: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 4]
            ),
            fontSize: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 10],
            ),
        };
    });

    return (
        <TouchableHighlight style={[styles.inputouter, validationStyle, style]} underlayColor="tranparent" onPress={focusinput}>
            <>
                <Animated.Text style={[labelStyle, styles.animlabel]}>{label}</Animated.Text>
                <TextInput
                    multiline={true}
                    ref={inputRef}
                    secureTextEntry={secureTextEntry === undefined ? false : secureTextEntry}
                    style={[styles.textinput, styles.addressinput]}
                    value={value}
                    onChangeText={onChangeText}
                    onFocus={handleFocus}
                    onBlur={(e) => handleBlur(e)}
                    keyboardType={keyboardType === undefined ? "default" : keyboardType}
                    autoCapitalize="none"
                />
            </>
        </TouchableHighlight>
    );
}

export default AddressInput;
