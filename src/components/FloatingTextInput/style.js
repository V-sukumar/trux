import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    inputouter: {
        borderWidth: 1,
        backgroundColor: "#EDF9FA",
        borderRadius: 10,
        height: 50,
        justifyContent: "flex-end"
    },
    textinput: {
        fontFamily: "Poppins-Regular",
        color: "#000",
        fontSize: 16,
        height: 36,
        paddingHorizontal: 10,
        paddingVertical: 0,
    },
    calendarinput: {
        paddingTop: 6,
    },
    animlabel: {
        position: 'absolute',
        fontFamily: "Poppins-Medium",
        color: "#9EA6A7",
    },
    addressinput: {
        height: 80,
        overflow: "hidden",
        textAlignVertical: "top",
    },
    calendaricon: {
        position: 'absolute',
        zIndex: 1,
        right: 0,
        height: 50,
        width: 50,
        top: 0,
        alignItems: "center",
        justifyContent: "center",
    },
});
