import React, { useState, useEffect } from 'react';
import { Text, TouchableHighlight, View, ScrollView } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle, withTiming, interpolate } from 'react-native-reanimated';
import { SvgXml } from 'react-native-svg';
import Modal from "react-native-modal";
import { downarrow } from "../../img/AuthIcon";
import { closeicon } from "../../img/AppIcon";
import { styles } from './style';
import { styles as cstyles } from '../CountryCode/style';

function SelectPicker(props) {
    const { label, setFieldValue, value, validationStyle, field, data, valuename, idname, setId } = props;
    const animateIsFocused = useSharedValue(value === '' ? 0 : 1);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        animateIsFocused.value = withTiming((value !== '') ? 1 : 0, { duration: 150 });
    }, [value]);

    function handlePicker() {
        setOpen(true);
    }

    function closeModal() {
        setOpen(false);
    }

    function setData(value, id) {
        setFieldValue(field, value);
        setId(id);
        closeModal();
    }

    const labelStyle = useAnimatedStyle(() => {
        return {
            left: 10,
            top: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 4]
            ),
            fontSize: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 10],
            ),
        };
    });

    return (
        <>
            <Modal
                isVisible={open}
                animationIn="fadeIn"
                animationOut="fadeOut"
                hideModalContentWhileAnimating={true}
                backdropTransitionOutTiming={0}
                backdropColor="#000"
                backdropOpacity={0.5}
                style={cstyles.modal}
            >
                <View style={cstyles.modalInner}>
                    <View style={cstyles.modalcont}>
                        <TouchableHighlight
                            style={cstyles.closeico}
                            onPress={closeModal}
                            underlayColor="transparent"
                        >
                            <SvgXml xml={closeicon} width="10" height="10" />
                        </TouchableHighlight>
                    </View>
                    <ScrollView
                        keyboardShouldPersistTaps="always"
                        contentContainerStyle={cstyles.scrollCont}
                    >
                        {data.map((item, i) =>
                            <TouchableHighlight
                                key={i}
                                onPress={() => setData(item[valuename], item[idname])}
                                style={cstyles.countrybtn}
                                underlayColor="transparent"
                            >
                                <Text style={cstyles.mobilecode}>{item[valuename]}</Text>
                            </TouchableHighlight>
                        )}
                    </ScrollView>
                </View>
            </Modal>
            <TouchableHighlight
                onPress={handlePicker}
                style={[styles.inputouter, validationStyle]}
                underlayColor="tranparent"
            >
                <>
                    <Animated.Text style={[labelStyle, styles.animlabel]}>
                        {label}
                    </Animated.Text>
                    <Text style={[styles.textinput, styles.calendarinput]}>{value}</Text>
                    <View style={styles.calendaricon}>
                        <SvgXml xml={downarrow} width="14" height="14" />
                    </View>
                </>
            </TouchableHighlight>
        </>
    );
}

export default SelectPicker;
