import React, { useState, useEffect } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle, withTiming, interpolate } from 'react-native-reanimated';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { SvgXml } from 'react-native-svg';
import { calendaricon } from "../../img/AppIcon";
import { styles } from './style';

function DateInput(props) {
    const { label, setFieldValue, value, validationStyle, data } = props;
    const animateIsFocused = useSharedValue(value === '' ? 0 : 1);
    const [dateModal, setDateModal] = useState(false);
    const datevalue = value ? new Date(value) : new Date();

    useEffect(() => {
        animateIsFocused.value = withTiming((value !== '') ? 1 : 0, { duration: 150 });
    }, [value]);

    function handleDatepicker() {
        setDateModal(true);
    }

    function cancelDatePicker() {
        setDateModal(false);
    }

    function onChangeDate(selectedDate) {
        setDateModal(false);
        const disdate = selectedDate.getFullYear() + '-' + (selectedDate.getMonth() + 1).toString().padStart(2, "0") + "-" + selectedDate.getDate().toString().padStart(2, "0");
        setFieldValue(data, disdate.toString());
    }

    const labelStyle = useAnimatedStyle(() => {
        return {
            left: 10,
            top: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 4]
            ),
            fontSize: interpolate(
                animateIsFocused.value,
                [0, 1],
                [14, 10],
            ),
        };
    });

    return (
        <>
        <DateTimePickerModal
            date={datevalue}
            isVisible={dateModal}
            mode="date"
            onConfirm={onChangeDate}
            onCancel={cancelDatePicker}
        />
        <TouchableHighlight
            onPress={handleDatepicker}
            style={[styles.inputouter, validationStyle]}
            underlayColor="tranparent"
        >
            <>            
            <Animated.Text style={[labelStyle, styles.animlabel]}>
                {label}
            </Animated.Text>
            <Text style={[styles.textinput, styles.calendarinput]}>{value}</Text>
            <View style={styles.calendaricon}>
                <SvgXml xml={calendaricon} width="25" height="25" />
            </View>
            </>
        </TouchableHighlight>
        </>
    );
}

export default DateInput;
