import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    safearea: {
        flex: 1,
        backgroundColor: "#fff",
    },
    keyboardView: {
        flex: 1,
    },
    scrollContainer: {
        padding: 20,
    },
    header: {
        height: 58,
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        paddingHorizontal: 20,
        alignItems: "flex-start",
        justifyContent: "center",
    },
    headertxt: {
        color: "#fff",
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
    },
    headingCont: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    addbtn: {
        backgroundColor: "#22AFBA",
        height: 40,
        width: 154,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 8,
    },
    addbtntxt: {
        color: "#fff",
        fontSize: 16,
        fontFamily: "Poppins-SemiBold",
    },
});
