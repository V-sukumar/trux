import React from 'react';
import { Text, Platform, KeyboardAvoidingView } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import { styles } from './style';

function AppHOCRefresh(Component, page) {
    return (props) => {
        const insets = useSafeAreaInsets();

        return (
            <SafeAreaView
                style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
            >
                <LinearGradient colors={['#087077', '#009FA5']} style={styles.header}>
                    <Text numberOfLines={1} style={styles.headertxt}>{page}</Text>
                </LinearGradient>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? 56 : 0}
                    style={styles.keyboardView}
                >
                    <Component {...props} />
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

export default AppHOCRefresh;
