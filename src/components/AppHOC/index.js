import React from 'react';
import { Text, Platform, ScrollView, KeyboardAvoidingView } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector } from "react-redux";
import { styles } from './style';

function AppHOC(Component, page) {
    return (props) => {
        const insets = useSafeAreaInsets();
        const userinfo = useSelector((state) => state.auth.userInfo);
        const transinfo = useSelector((state) => state.transporterprofile.profileInfo);

        function getPagename() {
            if (page === "Dashboard") {
                switch (userinfo?.user_type) {
                    case "transporter":
                        return transinfo.company_name;
                    case "driver":
                        return "DriverDashboard";
                    case "customer":
                        return "TruckBooking";
                    default:
                        return "ProfileStatus";
                }
            } else {
                return page;
            }
        }
        const pageName = getPagename();

        return (
            <SafeAreaView
                style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
            >
                <LinearGradient colors={['#087077', '#009FA5']} style={styles.header}>
                    <Text numberOfLines={1} style={styles.headertxt}>{pageName}</Text>
                </LinearGradient>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? 56 : 0}
                    style={styles.keyboardView}
                >
                    <ScrollView
                        contentContainerStyle={styles.scrollContainer}
                        keyboardShouldPersistTaps="always"
                    >
                        <Component {...props} />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

export default AppHOC;
