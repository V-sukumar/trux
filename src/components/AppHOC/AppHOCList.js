import React from 'react';
import { Text, Platform, TouchableHighlight, View } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import { styles } from './style';
import { rightarrow } from '../../img/AppIcon';

function AppHOCList(Component, page, button, addpage) {
    return (props) => {
        const insets = useSafeAreaInsets();
        const navigation = useNavigation();

        function navig() {
            navigation.navigate(addpage);
        }

        return (
            <SafeAreaView
                style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
            >
                <LinearGradient colors={['#087077', '#009FA5']} style={styles.header}>
                    <View style={styles.headingCont}>
                        <Text style={styles.headertxt}>{page}</Text>
                        {page !== "FAQ" &&
                            <TouchableHighlight style={styles.addbtn} onPress={navig} underlayColor="#22AFBA">
                                <>
                                    <Text style={styles.addbtntxt}>Add {button}</Text>
                                    <SvgXml xml={rightarrow} width="20" height="20" />
                                </>
                            </TouchableHighlight>
                        }
                    </View>
                </LinearGradient>
                <Component {...props} />
            </SafeAreaView>
        );
    }
}

export default AppHOCList;
