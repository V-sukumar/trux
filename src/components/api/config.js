import { API_URL } from "@env";
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from "../../store";
import { logout } from "../../store/auth/authSlice";

const instance = axios.create({
    baseURL: API_URL,
    timeout: 1000 * 1000,
    headers: {
        "Access-Control-Allow-Origin": "*",
    },
});
instance.interceptors.request.use(
    async (config) => {
        const token = await AsyncStorage.getItem("@token");
        if (token !== null && config.url !== "app/login" && config.url !== "customers/register" && config.url !== "app/verify-otp") {
            config.headers.Authorization = "Bearer " + JSON.parse(token);
        }
        return config;
    }
);
instance.interceptors.response.use(
    async (response) => {
        if (response.data.message === "Token is invalid!" || response.data.message === "User Deactivated !") {
            try {
                await AsyncStorage.clear();
                store.dispatch(logout());
            } catch (e) {
            }
        }
        return response;
    }
);

export default instance;
