import instance from "./config";

export function supportApi(data) {
    return instance.post("support", data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function loginApi(data) {
    return instance.post('app/login', data);
}

export function faqApi(data) {
    return instance.get(`faq`);
}

export function trucklistApi(data) {
    return instance.get(`trucks?page=${data.page}&per_page=10`);
}

export async function addtruckApi(data) {
    return await instance.post('trucks', data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function updateTruckApi(data) {
    return instance.put(`trucks/${data.id}`, data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function driverlistApi(data) {
    return instance.get(`drivers?page=${data.page}&per_page=10`);
}

export function addDriverApi(data) {
    return instance.post('drivers', data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function updateDriverApi(data) {
    return instance.put(`drivers/${data.id}`, data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function customerRegisterApi(data) {
    return instance.post('customers/register', data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function transporterRegisterApi(data) {
    return instance.post('transporters/register', data, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function transporterprofileApi(data) {
    return instance.get(`transporters/${data.ref_id}/profile`);
}

export function transupdateprofileApi(data) {
    return instance.put(`transporters/${data.ref_id}`, data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function customerprofileApi(data) {
    return instance.get(`customers/${data.ref_id}/profile`);
}

export function custupdateprofileApi(data) {
    return instance.put(`customers/${data.ref_id}`, data.formdata, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export function verifyOtpApi(data) {
    return instance.post("app/verify-otp", data);
}

export function forgotpwdApi(data) {
    return instance.post("app/resend-otp", data);
}

export function trucktypedApi() {
    return instance.get("master/truck-types");
}

export function privacypolicyApi() {
    return instance.get("master/privacy-policy");
}

export function termsconditionsApi() {
    return instance.get("master/terms-and-conditions");
}
