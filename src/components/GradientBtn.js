import React from "react";
import { StyleSheet, Text } from "react-native";
import LinearGradient from 'react-native-linear-gradient';

function GradientBtn(props) {
    const { name, disabled } = props;

    return (
        <LinearGradient colors={disabled === undefined ? ['#094B54', '#009FA5'] : disabled === false ? ['#094B54', '#009FA5'] : ['#959595', "#959595"]} style={styles.btn}>
            <Text style={styles.btntxt}>{name}</Text>
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    btn: {
        flex: 1,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
    },
    btntxt: {
        fontSize: 16,
        color: "#fff",
        fontFamily: "Poppins-SemiBold",
    },
});

export default GradientBtn;