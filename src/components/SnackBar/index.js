import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import Animated, {
    useSharedValue,
    useAnimatedStyle,
    withTiming,
    withSequence,
    withDelay,
} from 'react-native-reanimated';
import { styles } from './style';

function SnackBar(props) {
    const { visible, onHide, text } = props;
    const snackFade = useSharedValue(0);
    const animationOpacity = useAnimatedStyle(() => {
        return {
            opacity: snackFade.value
        }
    });

    useEffect(() => {
        let mounted = true;
        if (visible) {
            snackFade.value = withSequence(
                withTiming(1, { duration: 500 }),
                withDelay(2000, withTiming(0, { duration: 500 }))
            );
            if (mounted) {
                onHide();
            }
        }
        return () => {
            mounted = false;
        }
    }, [visible]);

    return (
        <Animated.View style={[styles.snackbar, animationOpacity]} pointerEvents={'none'}>
            <View style={styles.snackcont}><Text style={styles.snacktxt}>{text}</Text></View>
        </Animated.View>
    )
}

export default SnackBar;
