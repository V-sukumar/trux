import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    snackbar: {
        position: 'absolute',
        bottom: 20,
        width: ScreenWidth,
        alignItems: 'center'
    },
    snackcont: {
        backgroundColor: 'rgba(0, 0, 0, 0.9)',
        borderRadius: 30,
        height: 50,
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    snacktxt: {
        color: '#ffffff',
        fontSize: 14,
        fontFamily: "Poppins-Regular",
    },
});
