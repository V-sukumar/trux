import { StyleSheet } from 'react-native';
import { ScreenWidth } from '../Constants';

export const styles = StyleSheet.create({
    safearea: {
        flex: 1,
        backgroundColor: "#fff",
    },
    keyboardview: {
        flex: 1,
    },
    scrollview: {
        flexGrow: 1,
        alignItems: "center",
        justifyContent: "flex-end",
    },
    bgimg: {
        width: ScreenWidth,
        position: "absolute",
        top: 0,
    },
});
