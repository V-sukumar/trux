import React from "react";
import { KeyboardAvoidingView, Platform, ScrollView, Image } from "react-native";
import { SafeAreaView, useSafeAreaInsets } from "react-native-safe-area-context";
import { styles } from './style';

function AuthHOC(Component) {
    return (props) => {
        const insets = useSafeAreaInsets();

        return (
            <SafeAreaView
                style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
            >
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                    style={styles.keyboardview}
                >
                    <ScrollView 
                        keyboardShouldPersistTaps="always"
                        contentContainerStyle={styles.scrollview}
                    >
                        <Image
                            source={require("../../img/loginbg.png")}
                            style={styles.bgimg}
                        />
                        <Component {...props} />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

export default AuthHOC;
