import { StyleSheet } from 'react-native';
import { ScreenWidth } from '../Constants';

export const styles = StyleSheet.create({
    drawcont: {
        flexGrow: 1,
    },
    drawertop: {
        marginTop: -4,
        padding: 16,
        flexDirection: "row",
    },
    imgouter: {
        width: 80,
    },
    profilcont: {
        width: (ScreenWidth * .8) - 128,
        marginLeft: 16,
        justifyContent: "space-between",
    },
    profileimg: {
        height: 80,
        width: 80,
    },    
    profilename: {
        fontSize: 24,
        color: "#fff",
        fontFamily: "Poppins-SemiBold",
        lineHeight: 36,
    },
    profilemail: {
        fontSize: 14,
        color: "#fff",
        fontFamily: "Poppins-Medium",
        lineHeight: 21,  
    },
    editbtn: {
        height: 28,
        width: 98,
        backgroundColor: "#fff",
        borderRadius: 8,
        alignItems: "center",
        justifyContent: "center",
    },
    editbtntxt: {
        color: "#009EA4",
        fontSize: 12,
        fontFamily: "Poppins-SemiBold",
    },
    menubtn: {
        flexDirection: "row",
        alignItems: "center",
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: "#D2D2D2",
        paddingHorizontal: 10,
    },
    menutxt: {
        marginLeft: 10,
        fontSize: 16,
        fontFamily: "Poppins-Regular",
        color: "#084D56",
    },
    logoutbtn: {
        borderBottomWidth: 0,
    },
    logoutxt: {
        color: "#009CA2",
    },
    menucontainer: {
        justifyContent: "space-between",
        flex: 1,
    },
    menuinner: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
});
