import React from 'react';
import { View, Text, TouchableHighlight, Image } from "react-native";
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";
import { DrawerContentScrollView } from '@react-navigation/drawer';
import LinearGradient from 'react-native-linear-gradient';
import { SvgXml } from 'react-native-svg';
import { styles } from './style';
import { dashboard, myorders, myprofile, trucks, drivers, trackshipment, support, faq, privacypolicy, termscondition, logout, mycalendar } from '../../img/MenuIcon';
import { logout as signout } from "../../store/auth/authSlice";

function DrawerContent(props) {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const insets = useSafeAreaInsets();
    const userinfo = useSelector((state) => state.auth.userInfo);
    const transinfo = useSelector((state) => state.transporterprofile.profileInfo);
    const custinfo = useSelector((state) => state.customerprofile.profileInfo);

    function logouthandler() {
        navigation.dispatch(DrawerActions.closeDrawer());
        setTimeout(() => {
            clearSession()
        }, 1000);
    }

    async function clearSession() {
        try {
            await AsyncStorage.clear();
            dispatch(signout());
        } catch (e) {
        }
    }

    function navig(page) {
        navigation.navigate(page);
    }

    function usertypenavig(page) {
        if (userinfo?.customer_type === "individual") {
            navigation.navigate("CustomerIndividualProfile");
        } else if (userinfo?.is_admin_verified === false || userinfo?.is_trade_doc_available === false) {
            if (userinfo?.user_type === "transporter") {
                navigation.navigate("TransporterProfile");
            } else if (userinfo?.user_type === "customer") {
                if (userinfo?.customer_type === "entity") {
                    navigation.navigate("CustomerEntityProfile");
                }
            }
        } else {
            navigation.navigate(page);
        }
    }

    return (
        <DrawerContentScrollView
            {...props}
            keyboardShouldPersistTaps="always"
            contentContainerStyle={[styles.drawcont, { marginTop: -(insets.top) }]}
        >
            <LinearGradient colors={['#094B54', '#009FA5']} style={styles.drawertop}>
                <View style={styles.imgouter}>
                    <Image source={require("../../img/profile.png")} style={styles.profileimg} />
                </View>
                {userinfo?.user_type === "transporter" &&
                    <View style={styles.profilcont}>
                        <Text numberOfLines={1} style={styles.profilename}>{transinfo?.company_name}</Text>
                        <Text numberOfLines={1} style={styles.profilemail}>{transinfo?.email}</Text>
                        <TouchableHighlight style={styles.editbtn} underlayColor="#fff" onPress={() => usertypenavig("TransporterProfile")}>
                            <Text style={styles.editbtntxt}>Edit Profile</Text>
                        </TouchableHighlight>
                    </View>
                }
                {userinfo?.user_type === "customer" &&
                    <View style={styles.profilcont}>
                        <Text numberOfLines={1} style={styles.profilename}>{custinfo?.company_name || custinfo?.name}</Text>
                        <Text numberOfLines={1} style={styles.profilemail}>{custinfo?.email}</Text>
                        <TouchableHighlight style={styles.editbtn} underlayColor="#fff" onPress={() => usertypenavig("CustomerEntityProfile")}>
                            <Text style={styles.editbtntxt}>Edit Profile</Text>
                        </TouchableHighlight>
                    </View>
                }
            </LinearGradient>
            <View style={styles.menucontainer}>
                <View style={styles.menuinner}>
                    {userinfo?.user_type === "transporter" &&
                        <>
                            <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("TransporterDashboard")}>
                                <>
                                    <SvgXml xml={dashboard} width="20" height="20" />
                                    <Text style={styles.menutxt}>Dashboard</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn}>
                                <>
                                    <SvgXml xml={myorders} width="20" height="20" />
                                    <Text style={styles.menutxt}>My Orders</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("TransporterProfile")}>
                                <>
                                    <SvgXml xml={myprofile} width="20" height="20" />
                                    <Text style={styles.menutxt}>My Profile</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("Trucks")}>
                                <>
                                    <SvgXml xml={trucks} width="20" height="20" />
                                    <Text style={styles.menutxt}>Trucks</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("Drivers")}>
                                <>
                                    <SvgXml xml={drivers} width="20" height="20" />
                                    <Text style={styles.menutxt}>Drivers</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn}>
                                <>
                                    <SvgXml xml={trackshipment} width="20" height="20" />
                                    <Text style={styles.menutxt}>Track Shipment</Text>
                                </>
                            </TouchableHighlight>
                        </>
                    }
                    {userinfo?.user_type === "driver" &&
                        <TouchableHighlight style={styles.menubtn}>
                            <>
                                <SvgXml xml={mycalendar} width="20" height="20" />
                                <Text style={styles.menutxt}>My Calendar</Text>
                            </>
                        </TouchableHighlight>
                    }
                    {userinfo?.user_type === "customer" &&
                        <>
                            <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("TruckBooking")}>
                                <>
                                    <SvgXml xml={trucks} width="20" height="20" />
                                    <Text style={styles.menutxt}>Book Truck</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.menubtn}>
                                <>
                                    <SvgXml xml={trackshipment} width="20" height="20" />
                                    <Text style={styles.menutxt}>Address Book</Text>
                                </>
                            </TouchableHighlight>
                        </>
                    }
                    <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("Support")}>
                        <>
                            <SvgXml xml={support} width="20" height="20" />
                            <Text style={styles.menutxt}>Support</Text>
                        </>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("FAQ")}>
                        <>
                            <SvgXml xml={faq} width="20" height="20" />
                            <Text style={styles.menutxt}>FAQ</Text>
                        </>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("PrivacyPolicy")}>
                        <>
                            <SvgXml xml={privacypolicy} width="20" height="20" />
                            <Text style={styles.menutxt}>Privacy Policy</Text>
                        </>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.menubtn} underlayColor="transparent" onPress={() => navig("TermsCondition")}>
                        <>
                            <SvgXml xml={termscondition} width="20" height="20" />
                            <Text style={styles.menutxt}>Terms and Conditions</Text>
                        </>
                    </TouchableHighlight>
                </View>
                <View style={[styles.menuinner, { marginBottom: insets.bottom }]}>
                    <TouchableHighlight style={[styles.menubtn, styles.logoutbtn]} onPress={logouthandler} underlayColor="transparent">
                        <>
                            <SvgXml xml={logout} width="20" height="20" />
                            <Text style={[styles.menutxt, styles.logoutxt]}>Logout</Text>
                        </>
                    </TouchableHighlight>
                </View>
            </View>
        </DrawerContentScrollView>
    );
}

export default DrawerContent;
