import { StyleSheet } from 'react-native';
import { ScreenWidth } from '../../components/Constants';

export const styles = StyleSheet.create({
    heading: {
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 10,
    },
    totalcontainer: {
        paddingLeft: 20,
    },
    tripdetails: {
        backgroundColor: "#F3F3F3",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 90,
        borderLeftWidth: 6,
        borderLeftColor: "#22AFBA",
        borderRadius: 6,
    },
    paydetails: {
        borderLeftColor: "#017AFF",
    },
    totaltrips: {
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#3A3C3F",
        lineHeight: 36,
    },
    totaltriptxt: {
        fontSize: 12,
        fontFamily: "Poppins-Regular",
        color: "#3A3C3F",
        lineHeight: 14,
    },
    tripcontainer: {
        marginVertical: 24,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    triprow: {
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 16,
    },
    transtriprow: {
        width: ScreenWidth / 3 - 24,
    },
    drivertriprow: {
        width: ScreenWidth / 2 - 32,
    },
    ongoing: {
        backgroundColor: "#017AFF",
    },
    upcoming: {
        backgroundColor: "#0EB983",
    },
    completed: {
        backgroundColor: "#F78C2A",
    },
    triptype: {
        fontSize: 14,
        fontFamily: "Poppins-Medium",
        color: "#fff",
    },
    tripcount: {
        color: "#fff",
        fontFamily: "Poppins-SemiBold",
        fontSize: 24,
        marginTop: 10,
    },
    tripicon: {
        height: 39,
        width: 39,
        backgroundColor: "#fff",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 14,
    },
    amtcontainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 20,
    },
    amtinner: {
        width: ScreenWidth / 2 - 30,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 6,
        height: 100,
    },
    amtsettled: {
        backgroundColor: "#F0F7FF",
    },
    amtpending: {
        backgroundColor: "#FFF4F4",
    },
    amountxt: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 30,
    },
    settledtxt: {
        color: "#017AFF",
    },
    pendingtxt: {
        color: "#F78C2A",
    },
    totalearnings: {
        color: "#0EB983",
    },
    omrcontainer: {
        flexDirection: "row",
        alignItems: "baseline",
    },
    omr: {
        marginLeft: 6,
    },
    ratingcontainer: {
        backgroundColor: "#F3F3F3",
        height: 58,
        alignItems: "center",
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderRadius: 5,
        flexDirection: "row",
        marginBottom: 20,
    },
    starcontainer: {
        flexDirection: "row",
        paddingHorizontal: 10,
    },
    ratingtxt: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 18,
        color: "#084D56",
    },
    stars: {
        marginHorizontal: 1,
    },
});
