import React, { useEffect } from "react";
import { Text, View, Image } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppHOC from "../../components/AppHOC";
import { profileFetch } from "../../store/transporterprofile/transporterprofileSlice";
import { styles } from "./style";
import { ongoingtrip, upcomingtrip, completedtrip, starfull, starhalf } from "../../img/AppIcon";
import Loader from "../../components/Loader";
import { authUpdate } from "../../store/auth/authSlice";

function TransporterDashboard() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const isLoading = useSelector((state) => state.transporterprofile.isLoading);
    const profileinfo = useSelector((state) => state.transporterprofile.profileInfo);
    const userinfo = useSelector((state) => state.auth.userInfo);

    useEffect(() => {
        if (Object.keys(profileinfo).length === 0) {
            dispatch(profileFetch({ ref_id: userinfo?.ref_id }));
        }
    }, []);

    useEffect(() => {
        if (Object.keys(profileinfo).length !== 0) {
            if ((userinfo?.is_admin_verified !== profileinfo?.is_admin_verified) ||
                (userinfo?.is_trade_doc_available !== profileinfo?.is_trade_doc_available)) {
                updateinfo();
            }
        }
    }, [profileinfo]);

    async function updateinfo() {
        try {
            let userinfo = await AsyncStorage.getItem('@userinfo');
            const datainfo = JSON.parse(userinfo);
            datainfo.is_admin_verified = profileinfo.is_admin_verified;
            datainfo.is_trade_doc_available = profileinfo.is_trade_doc_available;
            await AsyncStorage.setItem('@userinfo', JSON.stringify(datainfo));
            dispatch(authUpdate(datainfo));
            if (!profileinfo.is_admin_verified || !profileinfo.is_trade_doc_available) {
                navigation.reset({
                    index: 0,
                    routes: [{ name: "ProfileStatus" }],
                });
            }
        } catch (e) {
        }
    }

    return (
        <>
            <View style={styles.ratingcontainer}>
                <Text style={styles.ratingtxt}>Your Rating</Text>
                <View style={styles.starcontainer}>
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starhalf} width="18" height="18" />
                </View>
                <Text style={styles.ratingtxt}>{'4.5/5'}</Text>
            </View>
            <Text style={styles.heading}>Trip Details</Text>
            <View style={styles.tripdetails}>
                <View style={styles.totalcontainer}>
                    <Text style={styles.totaltrips}>45</Text>
                    <Text style={styles.totaltriptxt}>Total Trips</Text>
                </View>
                <Image source={require("../../img/truck.png")} />
            </View>
            <View style={styles.tripcontainer}>
                <View style={[styles.triprow, styles.transtriprow, styles.ongoing]}>
                    <View style={styles.tripicon}>
                        <SvgXml xml={ongoingtrip} width="20" height="20" />
                    </View>
                    <Text style={styles.triptype}>On Going</Text>
                    <Text style={styles.triptype}>Trips</Text>
                    <Text style={styles.tripcount}>17</Text>
                </View>
                <View style={[styles.triprow, styles.transtriprow, styles.upcoming]}>
                    <View style={styles.tripicon}>
                        <SvgXml xml={upcomingtrip} width="20" height="20" />
                    </View>
                    <Text style={styles.triptype}>Upcoming</Text>
                    <Text style={styles.triptype}>Trips</Text>
                    <Text style={styles.tripcount}>25</Text>
                </View>
                <View style={[styles.triprow, styles.transtriprow, styles.completed]}>
                    <View style={styles.tripicon}>
                        <SvgXml xml={completedtrip} width="20" height="20" />
                    </View>
                    <Text style={styles.triptype}>Completed</Text>
                    <Text style={styles.triptype}>Trips</Text>
                    <Text style={styles.tripcount}>8</Text>
                </View>
            </View>
            <Text style={styles.heading}>Payment Details</Text>
            <View style={[styles.tripdetails, styles.paydetails]}>
                <View style={styles.totalcontainer}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.totaltrips, styles.totalearnings]}>1650</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Total Earnings</Text>
                </View>
            </View>
            <View style={styles.amtcontainer}>
                <View style={[styles.amtinner, styles.amtsettled]}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.amountxt, styles.settledtxt]}>1000</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Amount Settled</Text>
                </View>
                <View style={[styles.amtinner, styles.amtpending]}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.amountxt, styles.pendingtxt]}>650</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Amount Pending</Text>
                </View>
            </View>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(TransporterDashboard, "Dashboard");