import React, { useEffect, useState } from "react";
import { Alert, Platform, TouchableHighlight } from "react-native";
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useRoute, useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Pdf from 'react-native-pdf';
import ReactNativeBlobUtil from 'react-native-blob-util';
import { SvgXml } from 'react-native-svg';
import { API_URL } from "@env";
import { styles } from "./style";
import Loader from "../../components/Loader";
import { closewhite } from "../../img/AppIcon";

function PdfViewer() {
    const insets = useSafeAreaInsets();
    const route = useRoute();
    const navigation = useNavigation();
    const { url } = route.params;
    const [pdfpath, setPdfpath] = useState(null);
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        getfile();
    }, []);

    async function getfile() {
        setLoading(true);
        try {
            const token = await AsyncStorage.getItem("@token");
            const response = await ReactNativeBlobUtil.fetch('GET', `${API_URL}${url}`, {
                Authorization: "Bearer " + JSON.parse(token),
            });
            const pdfBlob = await response.data;
            if (pdfBlob.length > 50) {
                const pdfFilePath = `data:application/pdf;base64,${pdfBlob}`;
                setPdfpath(pdfFilePath);
            } else {
                Alert.alert("Alert", "File not found", [{ text: "OK", onPress: () => navigation.goBack() }], { cancelable: false });
            }
            setLoading(false);
        } catch (e) {
            setLoading(false);
        }
    }

    function backnavig() {
        navigation.goBack()
    }

    return (
        <SafeAreaView
            style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
        >
            <TouchableHighlight style={styles.closebtn} onPress={backnavig} underlayColor="#094B54">
                <SvgXml xml={closewhite} width="16" height="16" />
            </TouchableHighlight>
            {pdfpath !== null &&
            <Pdf
                source={{ uri: pdfpath }}
                style={styles.pdf}
            />}
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </SafeAreaView>
    );
}

export default PdfViewer;
