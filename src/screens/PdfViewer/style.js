import { StyleSheet } from 'react-native';
import { ScreenWidth, ScreenHeight } from '../../components/Constants';

export const styles = StyleSheet.create({
    safearea: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    pdf: {
        flex: 1,
        width: ScreenWidth,
        height: ScreenHeight,
    },
    imgfile: {
        height: "100%",
        width: ScreenWidth,
    },
    closebtn: {
        position: "absolute",
        zIndex: 2,
        height: 44,
        width: 44,
        backgroundColor: "#094B54",
        borderRadius: 100,
        top: 2,
        left: 8,
        alignItems: "center",
        justifyContent: "center",
    }
});
