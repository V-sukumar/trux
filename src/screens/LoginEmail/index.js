import React, { useState, useCallback } from "react";
import { View, TouchableHighlight, Text, BackHandler } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from "react-redux";
import { SvgXml } from 'react-native-svg';
import GradientBtn from "../../components/GradientBtn";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "./style";
import { gmail, eyeoff, eyeon } from "../../img/AuthIcon";
import { authInit } from "../../store/auth/authSlice";
import Loader from "../../components/Loader";
import FloatingTextInput from "../../components/FloatingTextInput";

const validateschema = Yup.object().shape({
    email: Yup.string().required('Enter Email').email('Invalid email'),
    password: Yup.string().required("Enter Password"),
});

function LoginEmail() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.auth.isLoading);
    const error = useSelector((state) => state.auth.error);
    const [secureText, setSecuretext] = useState(true);
    const [passIcon, setPassicon] = useState(eyeoff);

    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                BackHandler.exitApp();
                return true;
            };
            const backpress = BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => backpress.remove();
        }, [])
    );

    function togglePassword() {
        let iconName = (secureText) ? eyeon : eyeoff;
        setSecuretext(!secureText);
        setPassicon(iconName);
    }

    function handlelogin(values) {
        const payload = {
            email: values.email,
            password: values.password,
            fcm_token: "",
        };
        dispatch(authInit(payload));
    }

    function navig(page) {
        navigation.navigate(page);
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Login</Text>
                {error !== "" && <Text style={styles.errortxt}>{error}</Text>}
                <Formik
                    validationSchema={validateschema}
                    initialValues={{ email: "", password: "" }}
                    onSubmit={values => handlelogin(values)}
                >
                    {({ handleChange, handleBlur, touched, errors, handleSubmit, values }) => (
                        <>
                            <View style={styles.fields}>
                                <View style={styles.fieldicon}>
                                    <SvgXml xml={gmail} width="24" height="24" />
                                    <View style={styles.separator} />
                                </View>
                                <FloatingTextInput
                                    keyboardType="email-address"
                                    style={styles.inputmail}
                                    validationStyle={touched.email && errors.email ? styles.invalid : styles.valid}
                                    label="Enter Your Email"
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    value={values.email}
                                    leftDistance={60}
                                />
                                {touched.email && errors.email && <Text style={styles.errtxt}>{errors.email}</Text>}
                            </View>
                            <View style={styles.forgotpwdcont}>
                                <TouchableHighlight onPress={() => navig("LoginMobile")} underlayColor="transparent">
                                    <Text style={styles.forgotpwd}>Use Mobile Number</Text>
                                </TouchableHighlight>
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    secureTextEntry={secureText}
                                    style={styles.pwdinput}
                                    validationStyle={touched.password && errors.password ? styles.invalid : styles.valid}
                                    label="Password"
                                    onChangeText={handleChange('password')}
                                    onBlur={handleBlur('password')}
                                    value={values.password}
                                />
                                <TouchableHighlight underlayColor="transparent" style={styles.pwdicon} onPress={togglePassword}>
                                    <SvgXml xml={passIcon} width="18" height="18" />
                                </TouchableHighlight>
                                {touched.password && errors.password && <Text style={styles.errtxt}>{errors.password}</Text>}
                            </View>
                            <View style={styles.forgotpwdcont}>
                                <TouchableHighlight underlayColor="transparent" onPress={() => navig("ForgotPassword")}>
                                    <Text style={styles.forgotpwd}>Forgot password?</Text>
                                </TouchableHighlight>
                            </View>
                            <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.gradientbtn}>
                                <GradientBtn name="Login" />
                            </TouchableHighlight>
                        </>
                    )}
                </Formik>
                <View style={styles.newreg}>
                    <View style={styles.newregheading}>
                        <View style={styles.bgline} />
                        <Text style={styles.newregtxt}>New User Register Here!</Text>
                    </View>
                    <View style={styles.btncont}>
                        <TouchableHighlight style={styles.newregbtn} underlayColor="transparent" onPress={() => navig("SigninCustomer")}>
                            <Text style={styles.newregbtntxt}>Customer</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.newregbtn} underlayColor="transparent" onPress={() => navig("SigninTransporter")}>
                            <Text style={styles.newregbtntxt}>Transporter</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
            <Loader isVisible={loading} />
        </View>
    );
}

export default AuthHOC(LoginEmail);
