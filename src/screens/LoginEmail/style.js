import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    viewcont: {
        backgroundColor: "#fff",
        alignItems: "center",
        borderTopLeftRadius: 30,
        borderTopEndRadius: 30,
        width: ScreenWidth,
        paddingBottom: 20,
    },
    formcont: {
        width: (ScreenWidth - 40),
    },
    heading: {
        marginTop: 20,
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 10,
    },
    fields: {
        position: 'relative',
    },
    fieldicon: {
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 6,
        height: 50,
        width: 50,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    fieldmobile: {
        left: 34,
    },
    separator: {
        width: 1,
        height: 24,
        backgroundColor: "#9EA6A7",
        marginLeft: 10,
    },
    pwdicon: {
        position: 'absolute',
        zIndex: 1,
        right: 0,
        height: 50,
        width: 50,
        top: 0,
        alignItems: "center",
        justifyContent: "center",
    },
    pwdinput: {
        paddingRight: 50,
    },
    flagdiv: {
        borderRadius: 100,
        overflow: "hidden",
    },
    mobilecode: {
        fontSize: 14,
        color: "#03353C",
        fontFamily: "Poppins-Regular",
        paddingHorizontal: 6,
    },
    inputmail: {
        paddingLeft: 60,
    },
    mobileinput: {
        paddingLeft: 120,
    },
    fieldmobileicons: {
        flexDirection: "row",
        alignItems: "center",
    },
    forgotpwdcont: {
        alignItems: "flex-end",
    },
    forgotpwd: {
        color: "#08525B",
        fontSize: 12,
        fontFamily: "Poppins-Medium",
        marginVertical: 10,
    },
    invalid: {
        borderColor: '#ff0000',
    },
    valid: {
        borderColor: "#094B54",
    },
    errtxt: {
        color: '#ff0000',
        fontSize: 14,
        marginTop: 2,
        fontFamily: "Poppins-Regular",
    },
    newreg: {
        marginTop: 20,
    },
    newregheading: {
        alignItems: "center",
        marginBottom: 20,
        position: "relative",
    },
    bgline: {
        width: "96%",
        height: 1,
        backgroundColor: "#094B54",
        position: "absolute",
        top: 11,
    },
    newregtxt: {
        color: "#084D56",
        fontSize: 14,
        fontFamily: "Poppins-SemiBold",
        backgroundColor: "#fff",
        width: 210,
        textAlign: "center",
    },
    btncont: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    newregbtn: {
        height: 46,
        borderWidth: 1,
        borderColor: "#094B54",
        backgroundColor: "#EDEDED",
        borderRadius: 10,
        width: (ScreenWidth - 60) / 2,
        alignItems: "center",
        justifyContent: "center",
    },
    newregbtntxt: {
        color: "#084E56",
        fontSize: 12,
        fontFamily: "Poppins-SemiBold",
    },
    gradientbtn: {
        height: 50,
    },
    errortxt: {
        color: '#ff0000',
        fontSize: 16,
        textAlign: 'center',
        marginBottom: 6,
        fontFamily: "Poppins-Regular",
    },
});
