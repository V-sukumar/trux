import React, { useState, useEffect } from "react";
import { Text, View, TouchableHighlight, Keyboard } from "react-native";
import { useRoute } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { Formik } from 'formik';
import * as Yup from "yup";
import { SvgXml } from 'react-native-svg';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import GradientBtn from "../../components/GradientBtn";
import AppHOC from "../../components/AppHOC";
import FloatingTextInput from "../../components/FloatingTextInput";
import SelectPicker from "../../components/FloatingTextInput/SelectPicker";
import DateInput from "../../components/FloatingTextInput/DateInput";
import { styles } from "../AddDriver/style";
import { upload, camera, document, trash } from "../../img/AppIcon";
import { checkbox } from "../../img/AuthIcon";
import { trucktypelist, trucktypeFetch } from "../../store/trucktype/trucktypeSlice";
import { trucklist, updatetruckInit } from "../../store/truck/truckSlice";
import { MAX_FILE_SIZE } from "../../components/Constants";
import Loader from "../../components/Loader";

const validateschema = Yup.object().shape({
    compilances: Yup.array().min(1, "Select atleast one"),
    trucktype: Yup.string().required("Select Truck type"),
    truckno: Yup.string().required("Enter Truck no."),
    regexp: Yup.string().required("Enter Registration Expiry"),
    insurancevalidity: Yup.string().required("Enter Insurance Expiry"),
    regiscard: Yup.mixed().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }).required('Upload registration card'),
    insurancecard: Yup.mixed().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }).required('Upload Insurance card'),
    truckphoto: Yup.mixed().nullable().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value && value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }),
    carinsurance: Yup.mixed().nullable().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value && value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }),
});

function EditTruck() {
    const route = useRoute();
    const { id } = route.params;
    const dispatch = useDispatch();
    const userinfo = useSelector((state) => state.auth.userInfo);
    const isLoading = useSelector((state) => state.truck.isLoading);
    const trucks = useSelector(trucklist);
    const existdata = trucks.find(item => item.id === id);
    const tructype = useSelector(trucktypelist);
    const trucktypeid = tructype.find(item => item.truck_type === existdata?.truck_type);
    const [typetruck, setTrucktype] = useState(trucktypeid?.id);
    const regcardstate = existdata?.reg_card.length > 0 ? existdata?.reg_card : [];
    const regcardform = existdata?.reg_card.length > 0 ? existdata?.reg_card : null;
    const [regcard, setRegcard] = useState(regcardstate);
    const inscardstate = existdata?.insurance.length > 0 ? existdata?.insurance : [];
    const inscardform = existdata?.insurance.length > 0 ? existdata?.insurance : null;
    const [inscard, setInscard] = useState(inscardstate);
    const truckphotostate = existdata?.truck_photo !== null && existdata?.truck_photo.length > 0 ? existdata?.truck_photo : [];
    const truckphotoform = existdata?.truck_photo !== null && existdata?.truck_photo.length > 0 ? existdata?.truck_photo : null;
    const [phototruck, setPhototruck] = useState(truckphotostate);
    const carinstate = existdata?.cargo_insurance !== null && existdata?.cargo_insurance.length > 0 ? existdata?.cargo_insurance : [];
    const carinform = existdata?.cargo_insurance !== null && existdata?.cargo_insurance.length > 0 ? existdata?.cargo_insurance : null;
    const [carins, setCarins] = useState(carinstate);
    const [complaince, setComplaince] = useState(JSON.parse(existdata?.compilances));
    const [compCheck, setCompcheck] = useState({
        regular: complaince.includes("Regular"),
        foodgrade: complaince.includes("Food Grade"),
        oilandgas: complaince.includes("Oil and Gas"),
        hazardous: complaince.includes("Hazardous"),
    });

    useEffect(() => {
        if (tructype.length === 0) {
            dispatch(trucktypeFetch());
        }
    }, []);

    useEffect(() => {
        setTrucktype(trucktypeid?.id);
    }, [trucktypeid?.id]);

    async function handleFilePick(setFieldValue, file, setState) {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
                allowMultiSelection: true,
            });
            setFieldValue(file, res);
            setState(res);
        }
        catch (err) {
        }
    }

    function cameraOpen(setFieldValue, file, setState) {
        ImagePicker.openCamera({
            cropping: true,
        }).then(image => {
            setFieldValue(file, [image]);
            setState([image]);
        }).catch((e) => {
        });
    }

    function deleteFile(setFieldValue, file, setState) {
        setFieldValue(file, null);
        setState([]);
    }

    function updateComplaince(key, value, checkvalue, setFieldValue) {
        setCompcheck((prevCheck) => ({
            ...prevCheck,
            [key]: value,
        }));
        const newData = value ? [...complaince, checkvalue] : complaince.filter((item) => item !== checkvalue);
        setComplaince(newData);
        setFieldValue("compilances", newData);
    }

    function fileExt(ext) {
        switch (ext) {
            case "application/pdf":
                return "pdf";
            case "image/jpg":
            case "image/jpeg":
                return "jpg";
            case "image/png":
                return "png";
            default:
                return "jpg";
        }
    }

    function handleupdateTruck(values) {
        Keyboard.dismiss();
        var formdata = new FormData();
        formdata.append("truck_owner", userinfo?.ref_id);
        formdata.append("truck_type", typetruck);
        formdata.append("truck_no", values.truckno);
        formdata.append("reg_expiry_date", values.regexp);
        formdata.append("insurane_validity", values.insurancevalidity);
        formdata.append("compilances", JSON.stringify(complaince));
        let r = 1; let n = 1; let p = 1; let c = 1;
        let regarr = []; let insarr = []; let photoarr = []; let carinarr = [];
        if (existdata?.reg_card !== regcard) {
            if (regcard.length > 0) {
                regcard.map((item, i) => {
                    formdata.append('reg_card', {
                        uri: item.uri || item.path,
                        name: item.name || `reg_card_${i}.jpg`,
                        type: item.type || item.mime,
                    })
                    const regdata = { id: r, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                    r++;
                    regarr.push(regdata);
                });
            }
        } else {
            regarr.push(...existdata?.reg_card);
        }
        if (existdata?.insurance !== inscard) {
            if (inscard.length > 0) {
                inscard.map((item, i) => {
                    formdata.append('insurance', {
                        uri: item.uri || item.path,
                        name: item.name || `insurance_${i}.jpg`,
                        type: item.type || item.mime,
                    })
                    const insdata = { id: n, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                    n++;
                    insarr.push(insdata);
                });
            }
        } else {
            insarr.push(...existdata?.insurance);
        }
        if (existdata?.truck_photo !== phototruck) {
            if (phototruck.length > 0) {
                phototruck.map((item, i) => {
                    formdata.append('truck_photo', {
                        uri: item.uri || item.path,
                        name: item.name || `truck_photo_${i}.jpg`,
                        type: item.type || item.mime,
                    })
                    const photodata = { id: p, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                    p++;
                    photoarr.push(photodata);
                });
            }
        } else {
            photoarr.push(...existdata?.truck_photo);
        }
        if (existdata?.cargo_insurance !== carins) {
            if (carins.length > 0) {
                carins.map((item, i) => {
                    formdata.append('cargo_insurance', {
                        uri: item.uri || item.path,
                        name: item.name || `cargo_insurance_${i}.jpg`,
                        type: item.type || item.mime,
                    })
                    const cardata = { id: c, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                    c++;
                    carinarr.push(cardata);
                });
            }
        } else {
            carinarr.push(...existdata?.cargo_insurance);
        }
        const payload = {
            id,
            newdata: {
                cargo_insurance: carinarr,
                compilances: JSON.stringify(complaince),
                insurance: insarr,
                insurance_validity: values.insurancevalidity,
                reg_card: regarr,
                reg_expire_date: values.regexp,
                truck_no: values.truckno,
                truck_photo: photoarr,
                truck_type: values.trucktype
            },
            formdata,
        };
        dispatch(updatetruckInit(payload));
    }

    return (
        <>
            <Formik
                validationSchema={validateschema}
                initialValues={{ trucktype: existdata?.truck_type, truckno: existdata?.truck_no, regexp: existdata?.reg_expire_date, insurancevalidity: existdata?.insurance_validity, regiscard: regcardform, insurancecard: inscardform, truckphoto: truckphotoform, carinsurance: carinform, compilances: complaince }}
                onSubmit={values => handleupdateTruck(values)}
            >
                {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                    <>
                        <View style={styles.fields}>
                            <SelectPicker
                                label="Truck Type"
                                value={values.trucktype}
                                validationStyle={touched.trucktype && errors.trucktype ? styles.invalid : styles.valid}
                                setFieldValue={setFieldValue}
                                field="trucktype"
                                data={tructype}
                                valuename="truck_type"
                                idname="id"
                                setId={setTrucktype}
                            />
                            {touched.trucktype && errors.trucktype && <Text style={styles.errtxt}>{errors.trucktype}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.truckno && errors.truckno ? styles.invalid : styles.valid}
                                label="Truck No"
                                onChangeText={handleChange('truckno')}
                                onBlur={handleBlur('truckno')}
                                value={values.truckno}
                            />
                            {touched.truckno && errors.truckno && <Text style={styles.errtxt}>{errors.truckno}</Text>}
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Upload Your Registration Card</Text>
                            {regcard?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {regcard.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `reg_card_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "regiscard", setRegcard)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "regiscard", setRegcard)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "regiscard", setRegcard)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.regiscard && errors.regiscard && <Text style={styles.errtxt}>{errors.regiscard}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <DateInput
                                label="Registration Expiry Date"
                                onChangeText={handleChange('regexp')}
                                onBlur={handleBlur('regexp')}
                                value={values.regexp}
                                data="regexp"
                                validationStyle={touched.regexp && errors.regexp ? styles.invalid : styles.valid}
                                setFieldValue={setFieldValue}
                            />
                            {touched.regexp && errors.regexp && <Text style={styles.errtxt}>{errors.regexp}</Text>}
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Upload Your Insurance Card</Text>
                            {inscard?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {inscard.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `insurance_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "insurancecard", setInscard)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "insurancecard", setInscard)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "insurancecard", setInscard)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.insurancecard && errors.insurancecard && <Text style={styles.errtxt}>{errors.insurancecard}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <DateInput
                                label="Insurance Validity"
                                onChangeText={handleChange('insurancevalidity')}
                                onBlur={handleBlur('insurancevalidity')}
                                value={values.insurancevalidity}
                                data="insurancevalidity"
                                setFieldValue={setFieldValue}
                                validationStyle={touched.insurancevalidity && errors.insurancevalidity ? styles.invalid : styles.valid}
                            />
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Upload Your Truck Photo (Optional)</Text>
                            {phototruck?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {phototruck.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `truck_photo_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "truckphoto", setPhototruck)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "truckphoto", setPhototruck)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "truckphoto", setPhototruck)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.truckphoto && errors.truckphoto && <Text style={styles.errtxt}>{errors.truckphoto}</Text>}
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Cargo Insurance (Optional)</Text>
                            {carins?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {carins.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `cargo_insurance_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "carinsurance", setCarins)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "carinsurance", setCarins)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "carinsurance", setCarins)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.carinsurance && errors.carinsurance && <Text style={styles.errtxt}>{errors.carinsurance}</Text>}
                        </View>
                        <Text style={styles.heading}>Complainces</Text>
                        <View style={styles.compCheckOuter}>
                            <TouchableHighlight
                                style={styles.complainceCheckbox}
                                onPress={() => updateComplaince("regular", !compCheck.regular, "Regular", setFieldValue)}
                                underlayColor="transparent"
                            >
                                <>
                                    <View style={[styles.checkouter, styles.checkouterenabled]}>
                                        {compCheck.regular ? <SvgXml xml={checkbox} width="9" height="7" /> : null}
                                    </View>
                                    <Text style={[styles.complaincetxt, styles.complaincenabled]}>Regular</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight
                                disabled={compCheck.hazardous}
                                style={styles.complainceCheckbox}
                                onPress={() => updateComplaince("foodgrade", !compCheck.foodgrade, "Food Grade", setFieldValue)}
                                underlayColor="transparent"
                            >
                                <>
                                    <View style={[styles.checkouter, compCheck.hazardous ? styles.checkouterdisabled : styles.checkouterenabled]}>
                                        {compCheck.foodgrade ? <SvgXml xml={checkbox} width="9" height="7" /> : null}
                                    </View>
                                    <Text style={[styles.complaincetxt, compCheck.hazardous ? styles.complaincedisbled : styles.complaincenabled]}>Food Grade</Text>
                                </>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.compCheckOuter}>
                            <TouchableHighlight
                                style={styles.complainceCheckbox}
                                onPress={() => updateComplaince("oilandgas", !compCheck.oilandgas, "Oil and Gas", setFieldValue)}
                                underlayColor="transparent"
                            >
                                <>
                                    <View style={[styles.checkouter, styles.checkouterenabled]}>
                                        {compCheck.oilandgas ? <SvgXml xml={checkbox} width="9" height="7" /> : null}
                                    </View>
                                    <Text style={[styles.complaincetxt, styles.complaincenabled]}>Oil and Gas</Text>
                                </>
                            </TouchableHighlight>
                            <TouchableHighlight
                                disabled={compCheck.foodgrade}
                                style={styles.complainceCheckbox}
                                onPress={() => updateComplaince("hazardous", !compCheck.hazardous, "Hazardous", setFieldValue)} underlayColor="transparent"
                            >
                                <>
                                    <View style={[styles.checkouter, compCheck.foodgrade ? styles.checkouterdisabled : styles.checkouterenabled]}>
                                        {compCheck.hazardous ? <SvgXml xml={checkbox} width="9" height="7" /> : null}
                                    </View>
                                    <Text style={[styles.complaincetxt, compCheck.foodgrade ? styles.complaincedisbled : styles.complaincenabled]}>Hazardous</Text>
                                </>
                            </TouchableHighlight>
                        </View>
                        {touched.compilances && errors.compilances && <Text style={styles.errtxt}>{errors.compilances}</Text>}
                        <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                            <GradientBtn name="Submit" />
                        </TouchableHighlight>
                    </>
                )}
            </Formik>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(EditTruck, "Edit Truck Details");
