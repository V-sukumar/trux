import React, { useEffect } from "react";
import { Text, View, ScrollView, RefreshControl, TouchableHighlight } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppHOCRefresh from "../../components/AppHOC/AppHOCRefresh";
import { styles } from "./style";
import { document, tickcircle, notverified } from "../../img/AppIcon";
import { profileFetch } from "../../store/transporterprofile/transporterprofileSlice";
import { cusprofileFetch } from "../../store/customerprofile/customerprofileSlice";
import { authUpdate } from "../../store/auth/authSlice";
import Loader from "../../components/Loader";

function ProfileStatus() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const userinfo = useSelector((state) => state.auth.userInfo);
    const transinfo = useSelector((state) => state.transporterprofile.profileInfo);
    const custinfo = useSelector((state) => state.customerprofile.profileInfo);
    const transLoading = useSelector((state) => state.transporterprofile.isLoading);
    const custLoading = useSelector((state) => state.customerprofile.isLoading);

    useEffect(() => {
        getdata();
    }, []);

    useEffect(() => {
        if (Object.keys(transinfo).length !== 0) {
            if ((userinfo?.is_admin_verified !== transinfo?.is_admin_verified) ||
                (userinfo?.is_trade_doc_available !== transinfo?.is_trade_doc_available)) {
                updateinfo(transinfo, "TransporterDashboard");
            }
        }
    }, [transinfo]);

    useEffect(() => {
        if (Object.keys(custinfo).length !== 0) {
            if ((userinfo?.is_admin_verified !== custinfo?.is_admin_verified) ||
                (userinfo?.is_trade_doc_available !== custinfo?.is_trade_doc_available)) {
                updateinfo(custinfo, "TruckBooking");
            }
        }
    }, [custinfo]);

    function getdata() {
        if (userinfo?.user_type === "transporter") {
            dispatch(profileFetch({ ref_id: userinfo?.ref_id }));
        } else if (userinfo?.user_type === "customer") {
            dispatch(cusprofileFetch({ ref_id: userinfo?.ref_id }));
        }
    }

    async function updateinfo(data, page) {
        try {
            let userinfo = await AsyncStorage.getItem('@userinfo');
            const datainfo = JSON.parse(userinfo);
            datainfo.is_admin_verified = data.is_admin_verified;
            datainfo.is_due_diligence_completed = data.is_due_diligence_completed;
            datainfo.is_trade_doc_available = data.is_trade_doc_available;
            await AsyncStorage.setItem('@userinfo', JSON.stringify(datainfo));
            dispatch(authUpdate(datainfo));
            if (data.is_admin_verified && data.is_trade_doc_available) {
                setTimeout(() => {
                    navigation.reset({
                        index: 0,
                        routes: [{ name: page }],
                    });
                }, 2000);
            }
        } catch (e) {
        }
    }

    function navig() {
        if (userinfo?.user_type === "transporter") {
            navigation.navigate("TransporterProfile");
        } else if (userinfo?.user_type === "customer") {
            navigation.navigate("CustomerEntityProfile");
        }
    }

    return (
        <ScrollView
            contentContainerStyle={styles.scrollContainer}
            keyboardShouldPersistTaps="always"
            refreshControl={<RefreshControl refreshing={false} onRefresh={getdata} />}
        >
            <View style={styles.infoOuter}>
                <View style={styles.iconContainer}>
                    <SvgXml xml={document} width="20" height="20" />
                </View>
                <View style={styles.infoView}>
                    <Text style={styles.infotxt}>Trade license document.pdf</Text>
                </View>
                <View style={styles.iconContainer}>
                    <SvgXml xml={userinfo?.is_trade_doc_available ? tickcircle : notverified} width="20" height="20" />
                </View>
            </View>
            <View style={styles.infoOuter}>
                <View style={styles.iconContainer}>
                    <SvgXml xml={document} width="20" height="20" />
                </View>
                <View style={styles.infoView}>
                    <Text style={styles.infotxt}>Admin Verification Pending</Text>
                    <Text style={styles.infosubtxt}>Stay calm. The admin is yet to verify the docs.</Text>
                </View>
                <View style={styles.iconContainer}>
                    <SvgXml xml={userinfo?.is_admin_verified ? tickcircle : notverified} width="20" height="20" />
                </View>
            </View>
            <View style={styles.infoOuter}>
                <View style={styles.iconContainer}>
                    <SvgXml xml={document} width="20" height="20" />
                </View>
                <View style={styles.infoView}>
                    <Text style={styles.infotxt}>Due Diligence Certification Pending</Text>
                    <Text style={styles.infosubtxt}>Evaluation phase. Physical verification pending.</Text>
                </View>
                <View style={styles.iconContainer}>
                    <SvgXml xml={userinfo?.is_due_diligence_completed ? tickcircle : notverified} width="20" height="20" />
                </View>
            </View>
            <View style={styles.notesouter}>
                <Text style={styles.notestxt}>Please contact TRUX support to finalize the due diligence process and unlock additional benefits such as credit and more.</Text>
                <TouchableHighlight style={styles.gotobtn} onPress={navig} underlayColor="transparent">
                    <Text style={styles.gototxt}>Goto Profile</Text>
                </TouchableHighlight>
            </View>
            <Loader isVisible={transLoading || custLoading} backdropColor="transparent" />
        </ScrollView >
    );
}

export default AppHOCRefresh(ProfileStatus, "Profile Status");
