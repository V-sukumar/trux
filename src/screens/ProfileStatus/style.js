import { StyleSheet } from 'react-native';
import { ScreenWidth } from '../../components/Constants';

export const styles = StyleSheet.create({
    scrollContainer: {
        padding: 20,
    },
    infoOuter: {
        flexDirection: "row",
        borderWidth: 1,
        borderColor: "#CACACA",
        borderRadius: 6,
        height: 90,
        padding: 16,
        alignItems: "center",
        marginBottom: 20,
    },
    infotxt: {
        color: "#353535",
        fontSize: 14,
        fontFamily: "Poppins-Medium",
    },
    infosubtxt: {
        color: "#989692",
        fontSize: 12,
        fontFamily: "Poppins-Regular",
    },
    infoView: {
        width: "80%",
    },
    iconContainer: {
        width: "10%",
        alignItems: "center",
    },
    notesouter: {
        alignItems: "center",
        marginTop: 10,
    },
    notestxt: {
        width: "80%",
        color: "#151616",
        fontSize: 12,
        fontFamily: "Poppins-Medium",
        textAlign: "center",
        lineHeight: 22,
    },
    gotobtn: {
        height: 18,
        marginTop: 20,
    },
    gototxt: {
        fontSize: 12,
        lineHeight: 18,
        color: "#08525B",
        fontFamily: "Poppins-Medium",
    },
});
