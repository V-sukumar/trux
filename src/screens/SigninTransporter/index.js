import React from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../SigninCustomer/style";
import singleuser from "../../img/singleuser";
import multipleuser from "../../img/multipleuser";

function SigninTransporter() {
    const navigation = useNavigation();

    function navig(page) {
        navigation.navigate(page);
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Sign Up</Text>
                <Text style={styles.subtxt}>You would like to Register As?</Text>
                <View style={styles.registeras}>
                    <TouchableHighlight style={styles.signinbtn} underlayColor="transparent" onPress={() => navig("TransporterSingle")}>
                        <>
                        <SvgXml xml={singleuser} width="40" height="40" />
                        <Text style={styles.btntxt}>Single User</Text>
                        </>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.signinbtn} underlayColor="transparent" onPress={() => navig("TransporterMultiple")}>
                        <>
                        <SvgXml xml={multipleuser} width="40" height="40" />
                        <Text style={styles.btntxt}>Multiple User</Text>
                        </>
                    </TouchableHighlight>
                </View>
            </View>
        </View>
    );
}

export default AuthHOC(SigninTransporter);
