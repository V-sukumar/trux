import React, { useState, useEffect } from "react";
import { Text, View, TouchableHighlight, Keyboard } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { useSelector, useDispatch } from "react-redux";
import FloatingTextInput from "../../components/FloatingTextInput";
import { upload, camera, document, trash } from "../../img/AppIcon";
import CountryCode from "../../components/CountryCode";
import GradientBtn from "../../components/GradientBtn";
import { MAX_FILE_SIZE } from "../../components/Constants";
import { styles } from "../AddDriver/style";
import { styles as dstyles } from "../Drivers/style";
import AppHOC from "../../components/AppHOC";
import { updateInit } from "../../store/customerprofile/customerprofileSlice";
import Loader from "../../components/Loader";

const validateschema = Yup.object().shape({
    companyname: Yup.string().required("Enter Company Name"),
    mobileno: Yup.string().matches(/^\d{7,10}$/, 'Must be between 7 and 10 digit').required('Enter Mobile no.'),
    contactperson: Yup.string().required("Enter Contact person"),
    address: Yup.string().required("Enter Address"),
    licenseno: Yup.string().required("Enter License no."),
    licensecard: Yup.mixed().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }).required('Upload trade license'),
});

function CustomerEntityProfile() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const userinfo = useSelector((state) => state.auth.userInfo);
    const custinfo = useSelector((state) => state.customerprofile.profileInfo);
    const isLoading = useSelector((state) => state.customerprofile.isLoading);
    const licensestate = custinfo?.trade_license_doc !== null && custinfo?.trade_license_doc.length > 0 ? custinfo?.trade_license_doc : [];
    const licenseform = custinfo?.trade_license_doc !== null && custinfo?.trade_license_doc.length > 0 ? custinfo?.trade_license_doc : null;
    const [licensetrade, setLicensetrade] = useState(licensestate);

    useEffect(() => {
        setLicensetrade(custinfo?.trade_license_doc);
    }, [custinfo?.trade_license_doc]);

    async function handleFilePick(setFieldValue, file, setState) {
        const multiple = (file === "licensecard") ? true : false;
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
                allowMultiSelection: multiple,
            });
            setFieldValue(file, res);
            setState(res);
        }
        catch (err) {
        }
    }

    function cameraOpen(setFieldValue, file, setState) {
        ImagePicker.openCamera({
            cropping: true,
        }).then(image => {
            setFieldValue(file, [image]);
            setState([image]);
        }).catch((e) => {
        });
    }

    function deleteFile(setFieldValue, file, setState) {
        setFieldValue(file, null);
        setState([]);
    }

    function fileExt(ext) {
        switch (ext) {
            case "application/pdf":
                return "pdf";
            case "image/jpg":
            case "image/jpeg":
                return "jpg";
            case "image/png":
                return "png";
            default:
                return "jpg";
        }
    }

    function handleupdateProfile(values) {
        Keyboard.dismiss();
        var formdata = new FormData();
        formdata.append("company_name", values.companyname);
        formdata.append("email", custinfo?.email);
        formdata.append("contact_no", values.mobileno);
        formdata.append("name", values.contactperson);
        formdata.append("company_address", values.address);
        formdata.append("country_code", values.country_code);
        formdata.append("trade_license_no", values.licenseno);
        let l = 1; let licensearr = [];
        if (custinfo?.trade_license_doc !== licensetrade) {
            if (licensetrade.length > 0) {
                licensetrade.map((item, i) => {
                    formdata.append('trade_license_doc', {
                        uri: item.uri || item.path,
                        name: item.name || `trade_license_${i}.jpg`,
                        type: item.type || item.mime,
                    });
                    const licdata = { id: l, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                    l++;
                    licensearr.push(licdata);
                });
            }
        } else {
            licensearr.push(...custinfo?.trade_license_doc);
        }
        const payload = {
            ref_id: userinfo?.ref_id,
            newdata: {
                company_name: values.companyname,
                email: custinfo?.email,
                contact_no: values.mobileno,
                name: values.contactperson,
                company_address: values.address,
                country_code: values.country_code,
                trade_license_no: values.licenseno,
                trade_license_doc: licensearr,
            },
            formdata,
        };
        dispatch(updateInit(payload));
    }

    function clicknavig(id, ext, file) {
        const url = `customers/${userinfo?.ref_id}/${file}/${id}`;
        if (ext === "pdf") {
            navigation.navigate("PdfViewer", { url });
        } else {
            navigation.navigate("ImageViewer", { url });
        }
    }

    return (
        <>
            <Formik
                validationSchema={validateschema}
                initialValues={{ mobileno: custinfo?.contact_no.toString(), country_code: custinfo?.country_code, companyname: custinfo?.company_name, address: custinfo?.company_address, licenseno: custinfo?.trade_license_no, licensecard: licenseform, contactperson: custinfo?.name }}
                onSubmit={values => handleupdateProfile(values)}
            >
                {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                    <>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.companyname && errors.companyname ? styles.invalid : styles.valid}
                                label="Company Name"
                                onChangeText={handleChange('companyname')}
                                onBlur={handleBlur('companyname')}
                                value={values.companyname}
                            />
                            {touched.companyname && errors.companyname && <Text style={styles.errtxt}>{errors.companyname}</Text>}
                        </View>
                        <View style={[dstyles.inputOuter, dstyles.textInput]}>
                            <Text style={dstyles.inputHeading}>Email Id</Text>
                            <Text style={dstyles.inputValue}>{custinfo?.email}</Text>
                        </View>
                        <View style={styles.fields}>
                            <CountryCode
                                data="country_code"
                                value={values.country_code}
                                setFieldValue={setFieldValue}
                            />
                            <FloatingTextInput
                                keyboardType="number-pad"
                                style={styles.mobileinput}
                                validationStyle={touched.mobileno && errors.mobileno ? styles.invalid : styles.valid}
                                label="Enter Your Mobile no."
                                onChangeText={handleChange('mobileno')}
                                onBlur={handleBlur('mobileno')}
                                value={values.mobileno}
                                leftDistance={120}
                            />
                            {touched.mobileno && errors.mobileno && <Text style={styles.errtxt}>{errors.mobileno}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.contactperson && errors.contactperson ? styles.invalid : styles.valid}
                                label="Contact Person"
                                onChangeText={handleChange('contactperson')}
                                onBlur={handleBlur('contactperson')}
                                value={values.contactperson}
                            />
                            {touched.contactperson && errors.contactperson && <Text style={styles.errtxt}>{errors.contactperson}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.address && errors.address ? styles.invalid : styles.valid}
                                label="Company Address"
                                onChangeText={handleChange('address')}
                                onBlur={handleBlur('address')}
                                value={values.address}
                            />
                            {touched.address && errors.address && <Text style={styles.errtxt}>{errors.address}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.licenseno && errors.licenseno ? styles.invalid : styles.valid}
                                label="License Number"
                                onChangeText={handleChange('licenseno')}
                                onBlur={handleBlur('licenseno')}
                                value={values.licenseno}
                            />
                            {touched.licenseno && errors.licenseno && <Text style={styles.errtxt}>{errors.licenseno}</Text>}
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Trade/Commercial License</Text>
                            {licensetrade?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {licensetrade.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `trade_license_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                    {item.id &&
                                                        <TouchableHighlight
                                                            style={styles.clickbtn}
                                                            onPress={() => clicknavig(item.id, item.file_ext, "trade_license_doc")}
                                                            underlayColor="transparent"
                                                        >
                                                            <Text style={styles.clickbtntxt}>Click to view</Text>
                                                        </TouchableHighlight>
                                                    }
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "licensecard", setLicensetrade)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "licensecard", setLicensetrade)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "licensecard", setLicensetrade)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.licensecard && errors.licensecard && <Text style={styles.errtxt}>{errors.licensecard}</Text>}
                        </View>
                        <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                            <GradientBtn name="Submit" />
                        </TouchableHighlight>
                    </>
                )}
            </Formik>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(CustomerEntityProfile, "My Profile");