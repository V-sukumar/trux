import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    heading: {
        fontSize: 16,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 10,
    },
    fields: {
        position: 'relative',
        marginBottom: 20,
    },
    pwdicon: {
        position: 'absolute',
        zIndex: 1,
        right: 0,
        height: 50,
        width: 50,
        top: 0,
        alignItems: "center",
        justifyContent: "center",
    },
    pwdinput: {
        paddingRight: 50,
    },
    fieldicon: {
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 6,
        height: 50,
        width: 50,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    fieldmobileicons: {
        flexDirection: "row",
        alignItems: "center",
    },
    mobileinput: {
        paddingLeft: 120,
    },
    fieldmobile: {
        left: 34,
    },
    separator: {
        width: 1,
        height: 24,
        backgroundColor: "#9EA6A7",
        marginLeft: 10,
    },
    flagdiv: {
        borderRadius: 100,
        overflow: "hidden",
    },
    mobilecode: {
        fontSize: 14,
        color: "#03353C",
        fontFamily: "Poppins-Regular",
        paddingHorizontal: 6,
    },
    invalid: {
        borderColor: '#ff0000',
    },
    valid: {
        borderColor: "#094B54",
    },
    errtxt: {
        color: '#ff0000',
        fontSize: 14,
        marginTop: 2,
        fontFamily: "Poppins-Regular",
    },
    regbtn: {
        marginTop: 10,
        height: 50,
    },
    addressinput: {
        height: 100,
    },
    uploadOuter: {
        marginBottom: 20,
    },
    uploadContainer: {
        flexDirection: "row",
        justifyContent: "center",
        borderWidth: 1,
        borderColor: "#CACACA",
        borderRadius: 6,
        borderStyle: "dashed",
        marginBottom: 6,
        alignItems: "center",
    },
    uploadbtn: {
        alignItems: "center",
        justifyContent: "center",
        width: "50%",
        paddingVertical: 20,
    },
    clicktxt: {
        fontSize: 14,
        color: "#01989E",
        fontFamily: "Poppins-Medium",
        marginVertical: 6,
    },
    maxtxt: {
        fontSize: 12,
        fontFamily: "Poppins-Regular",
        color: "#353535",
    },
    compCheckOuter: {
        flexDirection: "row",
        marginBottom: 10,
    },
    complainceCheckbox: {
        flexDirection: "row",
        alignItems: "flex-start",
        width: 140,
    },
    complaincetxt: {
        fontSize: 14,
        fontFamily: "Poppins-Regular",        
        marginLeft: 10,
        flexWrap: 'wrap',
        flex: 1,
    },
    complaincenabled: {
        color: "#053A41",
    },
    complaincedisbled: {
        color: "#c4c4c4",
    },
    checkouter: {
        height: 15,
        width: 15,
        borderWidth: 1,        
        borderRadius: 2,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 2,
    },
    checkouterenabled: {
        borderColor: "#094B54",
        backgroundColor: "#EDF9FA",
    },
    checkouterdisabled: {
        borderColor: "#c4c4c4",
        backgroundColor: "#f6f6f6",
    },
    fileContainer: {
        borderWidth: 1,
        borderColor: "#CACACA",
        borderRadius: 6,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 6,
    },
    fileInner: {
        width: "90%",
        flexDirection: "row",
        marginVertical: 6,
    },
    iconContainer: {
        width: "10%",
        alignItems: "center",
    },
    infoView: {
        width: "90%",
    },
    infotxt: {
        color: "#353535",
        fontSize: 14,
        fontFamily: "Poppins-Medium",
    },
    uploadseparator: {
        width: 1,
        backgroundColor: "#D9D9D9",
        height: "80%",
    },
    clickbtn: {
        marginBottom: 10
    },
    clickbtntxt: {
        fontSize: 12,
        fontFamily: "Poppins-SemiBold",
        color: "#01989E",
    },
});
