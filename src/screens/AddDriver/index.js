import React, { useState } from "react";
import { Text, View, TouchableHighlight, Keyboard } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { SvgXml } from 'react-native-svg';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { useDispatch, useSelector } from "react-redux";
import GradientBtn from "../../components/GradientBtn";
import AppHOC from "../../components/AppHOC";
import FloatingTextInput from "../../components/FloatingTextInput";
import AddressInput from "../../components/FloatingTextInput/AddressInput";
import DateInput from "../../components/FloatingTextInput/DateInput";
import { styles } from "./style";
import { upload, camera, document, trash } from "../../img/AppIcon";
import { MAX_FILE_SIZE } from "../../components/Constants";
import { adddriverInit } from "../../store/driver/driverSlice";
import Loader from "../../components/Loader";
import CountryCode from "../../components/CountryCode";

const validateschema = Yup.object().shape({
    email: Yup.string().required("Enter Email").email('Invalid email'),
    drivername: Yup.string().required("Enter Driver Name"),
    mobileno: Yup.string().matches(/^\d{7,10}$/, 'Must be 7 to 10 digit').required('Enter Mobile no.'),
    licenseno: Yup.string().required("Enter license no."),
    licensevalidity: Yup.string().required("Enter license validity"),
    licensecard: Yup.mixed().test('fileSize', 'Max filesize 20 MB', (value) => {
        let totalSize = value.reduce((acc, file) => acc + file.size, 0);
        return totalSize <= MAX_FILE_SIZE;
    }).required('Upload license card'),
});

function AddDriver() {
    const dispatch = useDispatch();
    const [license, setLicense] = useState([]);
    const userinfo = useSelector((state) => state.auth.userInfo);
    const loading = useSelector((state) => state.driver.isLoading);

    async function handleFilePick(setFieldValue, file, setState) {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
                allowMultiSelection: true,
            });
            setFieldValue(file, res);
            setState(res);
        }
        catch (err) {
        }
    }

    function cameraOpen(setFieldValue, file, setState) {
        ImagePicker.openCamera({
            cropping: true,
        }).then(image => {
            setFieldValue(file, [image]);
            setState([image]);
        }).catch((e) => {
        });
    }

    function deleteFile(setFieldValue, file, setState) {
        setFieldValue(file, null);
        setState([]);
    }

    function fileExt(ext) {
        switch (ext) {
            case "application/pdf":
                return "pdf";
            case "image/jpg":
            case "image/jpeg":
                return "jpg";
            case "image/png":
                return "png";
            default:
                return "jpg";
        }
    }

    function handleaddDriver(values) {
        Keyboard.dismiss();
        var formdata = new FormData();
        formdata.append("transporter", userinfo?.ref_id);
        formdata.append("name", values.drivername);
        formdata.append("country_code", values.countrycode);
        formdata.append("contact_no", values.mobileno);
        formdata.append("email", values.email);
        formdata.append("address", values.address);
        formdata.append("license_no", values.licenseno);
        formdata.append("license_validity", values.licensevalidity);
        let l = 1; let licensearr = [];
        if (license.length > 0) {
            license.map((item, i) => {
                formdata.append('license_doc', {
                    uri: item.uri || item.path,
                    name: item.name || `license_card_${i}.jpg`,
                    type: item.type || item.mime,
                });
                const licdata = { id: l, size: 1, file_ext: (item.type !== undefined ? fileExt(item.type) : fileExt(item.mime)) };
                l++;
                licensearr.push(licdata);
            });
        }
        const payload = {
            newdata: {
                address: values.address,
                contact_no: values.mobileno,
                country_code: values.countrycode,
                email: values.email,
                is_license_doc_verified: false,
                license_doc: licensearr,
                license_no: values.licenseno,
                license_validity: values.licensevalidity,
                name: values.drivername,
                status: "In Active",
            },
            formdata,
        };
        dispatch(adddriverInit(payload));
    }

    return (
        <>
            <Formik
                validationSchema={validateschema}
                initialValues={{ drivername: "", address: "", email: "", mobileno: "", countrycode: "971", licenseno: "", licensevalidity: "", licensecard: null }}
                onSubmit={values => handleaddDriver(values)}
            >
                {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                    <>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.drivername && errors.drivername ? styles.invalid : styles.valid}
                                label="Driver Name"
                                onChangeText={handleChange('drivername')}
                                onBlur={handleBlur('drivername')}
                                value={values.drivername}
                            />
                            {touched.drivername && errors.drivername && <Text style={styles.errtxt}>{errors.drivername}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <CountryCode
                                data="countrycode"
                                value={values.countrycode}
                                setFieldValue={setFieldValue}
                            />
                            <FloatingTextInput
                                keyboardType="number-pad"
                                style={styles.mobileinput}
                                validationStyle={touched.mobileno && errors.mobileno ? styles.invalid : styles.valid}
                                label="Enter Your Number"
                                onChangeText={handleChange('mobileno')}
                                onBlur={handleBlur('mobileno')}
                                value={values.mobileno}
                                leftDistance={120}
                            />
                            {touched.mobileno && errors.mobileno && <Text style={styles.errtxt}>{errors.mobileno}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                keyboardType="email-address"
                                label="Email Id"
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                                validationStyle={touched.email && errors.email ? styles.invalid : styles.valid}
                            />
                            {touched.email && errors.email && <Text style={styles.errtxt}>{errors.email}</Text>}
                        </View>
                        <Text style={styles.heading}>Enter Driver Address</Text>
                        <View style={styles.fields}>
                            <AddressInput
                                style={styles.addressinput}
                                label="Add your Address"
                                onChangeText={handleChange('address')}
                                onBlur={handleBlur('address')}
                                value={values.address}
                                validationStyle={styles.valid}
                            />
                        </View>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.licenseno && errors.licenseno ? styles.invalid : styles.valid}
                                label="License no."
                                onChangeText={handleChange('licenseno')}
                                onBlur={handleBlur('licenseno')}
                                value={values.licenseno}
                            />
                            {touched.licenseno && errors.licenseno && <Text style={styles.errtxt}>{errors.licenseno}</Text>}
                        </View>
                        <View style={styles.uploadOuter}>
                            <Text style={styles.heading}>Upload Driver License Card</Text>
                            {license?.length > 0 ?
                                <View style={styles.fileContainer}>
                                    <View>
                                        {license.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>
                                                        {item.name || `license_card_${i}.${item.file_ext || "jpg"}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    <View style={styles.iconContainer}>
                                        <TouchableHighlight underlayColor="transparent" onPress={() => deleteFile(setFieldValue, "licensecard", setLicense)}>
                                            <SvgXml xml={trash} width="20" height="20" />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                :
                                <View style={styles.uploadContainer}>
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => handleFilePick(setFieldValue, "licensecard", setLicense)}>
                                        <>
                                            <SvgXml xml={upload} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                    <View style={styles.uploadseparator} />
                                    <TouchableHighlight style={styles.uploadbtn} underlayColor="transparent" onPress={() => cameraOpen(setFieldValue, "licensecard", setLicense)}>
                                        <>
                                            <SvgXml xml={camera} width="44" height="44" />
                                            <Text style={styles.clicktxt}>Click to Upload</Text>
                                            <Text style={styles.maxtxt}>(Max. File size: 20 MB)</Text>
                                        </>
                                    </TouchableHighlight>
                                </View>
                            }
                            {touched.licensecard && errors.licensecard && <Text style={styles.errtxt}>{errors.licensecard}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <DateInput
                                label="License Validity"
                                onChangeText={handleChange('licensevalidity')}
                                onBlur={handleBlur('licensevalidity')}
                                value={values.licensevalidity}
                                data="licensevalidity"
                                setFieldValue={setFieldValue}
                                validationStyle={touched.licensevalidity && errors.licensevalidity ? styles.invalid : styles.valid}
                            />
                            {touched.licensevalidity && errors.licensevalidity && <Text style={styles.errtxt}>{errors.licensevalidity}</Text>}
                        </View>
                        <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                            <GradientBtn name="Add Driver" />
                        </TouchableHighlight>
                    </>
                )}
            </Formik>
            <Loader isVisible={loading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(AddDriver, "Add Driver");
