import React, { useCallback } from "react";
import { View, TouchableHighlight, Text, BackHandler } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import LottieView from "lottie-react-native";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../Otp/style";
import GradientBtn from "../../components/GradientBtn";
import * as RootNavigation from "../../components/RootNavigation";

function Thankyou() {
    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                RootNavigation.navigate("LoginEmail");
                return true;
            };
            const backpress = BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => backpress.remove();
        }, [])
    );

    function returnHome() {
        RootNavigation.navigate("LoginEmail");
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <View style={styles.lottiecontainer}>
                    <View style={styles.lottiecont}>
                        <LottieView source={require("../../img/success.json")} autoPlay loop={false} style={styles.lottieview} />
                    </View>
                </View>                
                <Text style={styles.thanktxt}>Thank you for your interest in us. You're just a few</Text>
                <Text style={styles.thanktxt}>steps away from enjoying our services. You'll be</Text>
                <Text style={styles.thanktxt}>notified as soon as you're activated by Trux.</Text>
                <TouchableHighlight underlayColor="transparent" onPress={returnHome} style={styles.contbtn}>
                    <GradientBtn name="Continue" />
                </TouchableHighlight>
            </View>
        </View>
    );
}

export default AuthHOC(Thankyou);
