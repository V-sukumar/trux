import React, { useState } from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { SvgXml } from 'react-native-svg';
import GradientBtn from "../../components/GradientBtn";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../CustomerIndividual/style";
import { eyeoff } from "../../img/AuthIcon";
import FloatingTextInput from "../../components/FloatingTextInput";

const validateschema = Yup.object().shape({
    newpass: Yup.string().required('Enter Password'),
    confpass: Yup.string().oneOf([Yup.ref('newpass'), null], "Password don't match").required('Confirm Password'),
});

function ChangePassword() {
    const [secureText, setSecuretext] = useState(true);

    function togglePassword() {
        setSecuretext(!secureText);
    }

    function handlechangepwd(values) {
        console.log(values)
    }
   
    return (
        <View style={[styles.viewcont, styles.forgotpwdcont]}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Change Password</Text>
                <Formik
                    validationSchema={validateschema}
                    initialValues={{ newpass: "", confpass: "" }}
                    onSubmit={values => handlechangepwd(values)}
                >
                    {({ handleChange, handleBlur, touched, errors, handleSubmit, values }) => (
                    <>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            secureTextEntry={secureText}
                            style={styles.pwdinput}
                            validationStyle={touched.newpass && errors.newpass ? styles.invalid : styles.valid}
                            label="New Password"
                            onChangeText={handleChange('newpass')}
                            onBlur={handleBlur('newpass')}
                            value={values.newpass}
                        />
                        <TouchableHighlight underlayColor="transparent" style={styles.pwdicon} onPress={togglePassword}>
                            <SvgXml xml={eyeoff} width="18" height="18" />
                        </TouchableHighlight>
                        {touched.newpass && errors.newpass && <Text style={styles.errtxt}>{errors.newpass}</Text>}
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            secureTextEntry={secureText}
                            style={styles.pwdinput}
                            validationStyle={touched.confpass && errors.confpass ? styles.invalid : styles.valid}
                            label="Confirm Password"
                            onChangeText={handleChange('confpass')}
                            onBlur={handleBlur('confpass')}
                            value={values.confpass}
                        />
                        <TouchableHighlight underlayColor="transparent" style={styles.pwdicon} onPress={togglePassword}>
                            <SvgXml xml={eyeoff} width="18" height="18" />
                        </TouchableHighlight>
                        {touched.confpass && errors.confpass && <Text style={styles.errtxt}>{errors.confpass}</Text>}
                    </View>
                    <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                        <GradientBtn name="Submit" />
                    </TouchableHighlight>
                    </>
                    )}
                </Formik>
            </View>
        </View>
    );
}

export default AuthHOC(ChangePassword);
