import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    scrollContainer: {
        marginBottom: 20,
    },
    truckbtn: {
        height: 113,
        width: 175,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#EDF9FA",
        marginRight: 20,
        borderRadius: 10,
    },
    trucktxt: {
        color: "#085058",
        fontSize: 14,
        fontFamily: "Poppins-Medium",
        textAlign: "center"
    },
    activeborder: {
        borderWidth: 1,
        borderColor: "#085058",
    },
});
