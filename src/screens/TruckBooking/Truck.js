import React, { useState } from "react";
import { Text, View, TouchableHighlight, ScrollView } from "react-native";
import { SvgXml } from 'react-native-svg';
import { pickuptruck } from "../../img/AppIcon";
import { styles } from "./style"

const trucks = [
    { name: "Pickup Truck 3 Ton" },
    { name: "Pickup Truck - 3Ton - Non-Palletised" },
];

function Truck() {
    const [active, setActive] = useState(0);

    return (
        <ScrollView
            horizontal
            contentContainerStyle={styles.scrollContainer}
            showsHorizontalScrollIndicator={false}
        >
            {trucks?.map((item, i) => (
                <TouchableHighlight key={i} style={[styles.truckbtn, (i === active) ? styles.activeborder : null ]} onPress={() => setActive(i)} underlayColor="#EDF9FA">
                    <>
                    <SvgXml xml={pickuptruck} width="50" height="50" />
                    <Text style={styles.trucktxt}>{item.name}</Text>
                    </>
                </TouchableHighlight>
            ))}
        </ScrollView>
    );
}

export default Truck;