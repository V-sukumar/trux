import React, { useEffect } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import GradientBtn from "../../components/GradientBtn";
import AppHOC from "../../components/AppHOC";
import FloatingTextInput from "../../components/FloatingTextInput";
import { styles } from "../AddDriver/style";
import Truck from "./Truck";
import { cusprofileFetch } from "../../store/customerprofile/customerprofileSlice";

function TruckBooking() {
    const dispatch = useDispatch();
    const userinfo = useSelector((state) => state.auth.userInfo);

    useEffect(() => {
        dispatch(cusprofileFetch({ ref_id: userinfo?.ref_id }));
    }, []);

    function handlebooking(values) {
        console.log(values);
    }

    return (
        <Formik
            initialValues={{ origin: "", destination: "", datetime: "", complaincetrail: "", temperaturecelcius: "", descriptiongoods: "", noofpackages: "", cargoname: "", similartrips: "" }}
            onSubmit={values => handlebooking(values)}
        >
            {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                <>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={touched.origin && errors.origin ? styles.invalid : styles.valid}
                            label="Origin"
                            onChangeText={handleChange('origin')}
                            onBlur={handleBlur('origin')}
                            value={values.origin}
                        />
                        {touched.origin && errors.origin && <Text style={styles.errtxt}>{errors.origin}</Text>}
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={touched.destination && errors.destination ? styles.invalid : styles.valid}
                            label="Destination"
                            onChangeText={handleChange("destination")}
                            onBlur={handleBlur("destination")}
                            value={values.destination}
                        />
                        {touched.destination && errors.destination && <Text style={styles.errtxt}>{errors.destination}</Text>}
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={touched.datetime && errors.datetime ? styles.invalid : styles.valid}
                            label="Date and Time of Shipment"
                            onChangeText={handleChange("datetime")}
                            onBlur={handleBlur("datetime")}
                            value={values.datetime}
                        />
                        {touched.datetime && errors.datetime && <Text style={styles.errtxt}>{errors.datetime}</Text>}
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={touched.complaincetrail && errors.complaincetrail ? styles.invalid : styles.valid}
                            label="Select Compliances of Trials"
                            onChangeText={handleChange("complaincetrail")}
                            onBlur={handleBlur("complaincetrail")}
                            value={values.complaincetrail}
                        />
                        {touched.complaincetrail && errors.complaincetrail && <Text style={styles.errtxt}>{errors.complaincetrail}</Text>}
                    </View>
                    <Truck />
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={styles.valid}
                            label="Temperature(in celsius)"
                            onChangeText={handleChange("temperaturecelcius")}
                            onBlur={handleBlur("temperaturecelcius")}
                            value={values.temperaturecelcius}
                        />
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={styles.valid}
                            label="Description of Goods"
                            onChangeText={handleChange("descriptiongoods")}
                            onBlur={handleBlur("descriptiongoods")}
                            value={values.descriptiongoods}
                        />
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={styles.valid}
                            label="No of Packages"
                            onChangeText={handleChange("noofpackages")}
                            onBlur={handleBlur("noofpackages")}
                            value={values.noofpackages}
                        />
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={styles.valid}
                            label="Enter Cargo Name"
                            onChangeText={handleChange("cargoname")}
                            onBlur={handleBlur("cargoname")}
                            value={values.cargoname}
                        />
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            validationStyle={styles.valid}
                            label="No of Similar Trips"
                            onChangeText={handleChange("similartrips")}
                            onBlur={handleBlur("similartrips")}
                            value={values.similartrips}
                        />
                    </View>
                    <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                        <GradientBtn name="Submit" />
                    </TouchableHighlight>
                </>
            )}
        </Formik>
    );
}

export default AppHOC(TruckBooking, "Truck Booking");
