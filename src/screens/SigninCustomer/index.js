import React from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import AuthHOC from "../../components/AuthHOC";
import { styles } from "./style";
import individual from "../../img/individual";
import entity from "../../img/entity";

function SigninCustomer() {
    const navigation = useNavigation();

    function navig(page) {
        navigation.navigate(page);
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Sign Up</Text>
                <Text style={styles.subtxt}>You would like to Register As?</Text>
                <View style={styles.registeras}>
                    <TouchableHighlight style={styles.signinbtn} underlayColor="transparent" onPress={() => navig("CustomerIndividual")}>
                        <>
                        <SvgXml xml={individual} width="40" height="40" />
                        <Text style={styles.btntxt}>Individual</Text>
                        </>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.signinbtn} underlayColor="transparent" onPress={() => navig("CustomerEntity")}>
                        <>
                        <SvgXml xml={entity} width="40" height="40" />
                        <Text style={styles.btntxt}>Entity</Text>
                        </>
                    </TouchableHighlight>
                </View>
            </View>
        </View>
    );
}

export default AuthHOC(SigninCustomer);
