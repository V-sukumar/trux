import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    viewcont: {
        backgroundColor: "#fff",
        alignItems: "center",
        borderTopLeftRadius: 30,
        borderTopEndRadius: 30,
        width: ScreenWidth,
        paddingBottom: 50,
    },
    formcont: {
        width: (ScreenWidth - 40),
    },
    heading: {
        marginTop: 20,
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 6,
    },
    subtxt: {
        fontSize: 16,
        fontFamily: "Poppins-Regular",
        color: "#053A41",
    },
    signinbtn: {
        height: 122,
        borderWidth: 1,
        borderColor: "#094B54",
        backgroundColor:"#EDEDED",
        width: (ScreenWidth-60) / 2,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
    },
    btntxt: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 16,
        color: "#084E56",
        marginTop: 10,
    },
    registeras: {
        marginTop: 26,
        flexDirection: "row", 
        justifyContent: "space-between",
    },
});
