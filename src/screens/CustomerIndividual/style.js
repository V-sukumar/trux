import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    viewcont: {
        backgroundColor: "#fff",
        alignItems: "center",
        borderTopLeftRadius: 30,
        borderTopEndRadius: 30,
        width: ScreenWidth,
        paddingBottom: 20,
    },
    formcont: {
        width: (ScreenWidth - 40),
    },
    heading: {
        marginTop: 20,
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 10,
    },
    fields: {
        position:'relative',
        marginBottom: 20,
    },
    pwdicon: {
        position: 'absolute',
        zIndex:1,
        right: 0,
        height:50,
        width:50,
        top: 0,
        alignItems: "center",
        justifyContent: "center",
    },
    pwdinput: {
        paddingRight: 50,
    },
    fieldicon: {
        position: 'absolute',
        zIndex:1,
        top: 0,
        left: 6,
        height: 50,
        width: 50,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    fieldmobileicons: {
        flexDirection:"row",
        alignItems: "center",
    },
    mobileinput: {
        paddingLeft: 120,
    },
    fieldmobile: {
        left: 34,
    },
    separator: {
        width: 1,
        height: 24,
        backgroundColor: "#9EA6A7",
        marginLeft: 10,
    },
    flagdiv: {
        borderRadius: 100,
        overflow: "hidden",
    },
    mobilecode: {
        fontSize: 14,
        color: "#03353C",
        fontFamily: "Poppins-Regular",
        paddingHorizontal: 6,
    },
    termscond: {
        flexDirection: "row",
        alignItems: "flex-start",
        marginBottom: 10,
    },
    termstxt: {
        fontSize: 14,
        fontFamily: "Poppins-Regular",
        color: "#053A41",
        marginLeft: 10,
        flexWrap: 'wrap',
        flex: 1,
    },
    checkouter: {
        height: 15,
        width: 15,
        borderWidth: 1,
        borderColor: "#094B54",
        backgroundColor: "#EDF9FA",
        borderRadius: 2,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 2,
    },
    invalid: {
        borderColor: '#ff0000',
    },
    valid: {
        borderColor: "#094B54",
    },
    errtxt: {
        color: '#ff0000',
        fontSize: 14,
        marginTop: 2,
        fontFamily: "Poppins-Regular",
    },
    regbtn: {
        marginTop: 10,
        height: 50,
    },
    forgotpwdcont: {
        paddingBottom: 50,
    },
    errortxt: {
        color: '#ff0000',
        fontSize: 16,
        textAlign: 'center',
        marginBottom: 6,
        fontFamily: "Poppins-Regular",
    },
});
