import React, { useState } from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { Formik } from 'formik';
import * as Yup from "yup";
import { SvgXml } from 'react-native-svg';
import GradientBtn from "../../components/GradientBtn";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../CustomerIndividual/style";
import { eyeoff, eyeon, checkbox } from "../../img/AuthIcon";
import FloatingTextInput from "../../components/FloatingTextInput";
import CountryCode from "../../components/CountryCode";
import { transregisterInit } from "../../store/transportersignup/transportersignupSlice";
import Loader from "../../components/Loader";

const validateschema = Yup.object().shape({
    companyname: Yup.string().required("Enter Company Name"),
    email: Yup.string().required("Enter Email").email('Invalid email'),
    password: Yup.string().required("Enter Password"),
    mobileno: Yup.string().matches(/^[0-9]{10}$/, "Enter 10 digit mobile no.").required('Enter Mobile no.'),
});

function TransporterSingle() {
    const dispatch = useDispatch();
    const [secureText, setSecuretext] = useState(true);
    const [passIcon, setPassicon] = useState(eyeoff);
    const [acceptTerms, setAcceptTerms] = useState(false);
    const transLoading = useSelector((state) => state.transportersignup.isLoading);
    const error = useSelector((state) => state.transportersignup.error);

    function togglePassword() {
        let iconName = (secureText) ? eyeon : eyeoff;
        setSecuretext(!secureText);
        setPassicon(iconName);
    }

    function handleregister(values) {
        var formdata = new FormData();
        formdata.append("type", "singleuser");
        formdata.append("company_name", values.companyname);
        formdata.append("company_address", values.address);
        formdata.append("contact_person", values.contactperson);
        formdata.append("email", values.email);
        formdata.append("password", values.password);
        formdata.append("country_code", values.countrycode);
        formdata.append("contact_no", values.mobileno);
        formdata.append("trade_license_no", values.licenseno);
        dispatch(transregisterInit({ formdata, email: values.email }));
    }

    function termsaccept() {
        setAcceptTerms(!acceptTerms);
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Single User</Text>
                {error !== "" && <Text style={styles.errortxt}>{error}</Text>}
                <Formik
                    validationSchema={validateschema}
                    initialValues={{ companyname: "", address: "", contactperson: "", email: "", password: "", mobileno: "", licenseno: "", countrycode: "971" }}
                    onSubmit={values => handleregister(values)}
                >
                    {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                        <>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    validationStyle={touched.companyname && errors.companyname ? styles.invalid : styles.valid}
                                    label="Company Name"
                                    onChangeText={handleChange('companyname')}
                                    onBlur={handleBlur('companyname')}
                                    value={values.companyname}
                                />
                                {touched.companyname && errors.companyname && <Text style={styles.errtxt}>{errors.companyname}</Text>}
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    label="Company Address"
                                    onChangeText={handleChange('address')}
                                    onBlur={handleBlur('address')}
                                    value={values.address}
                                />
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    label="Conatct Person"
                                    onChangeText={handleChange('contactperson')}
                                    onBlur={handleBlur('contactperson')}
                                    value={values.contactperson}
                                />
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    keyboardType="email-address"
                                    validationStyle={touched.email && errors.email ? styles.invalid : styles.valid}
                                    label="Email Address"
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    value={values.email}
                                />
                                {touched.email && errors.email && <Text style={styles.errtxt}>{errors.email}</Text>}
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    secureTextEntry={secureText}
                                    style={styles.pwdinput}
                                    validationStyle={touched.password && errors.password ? styles.invalid : styles.valid}
                                    label="Password"
                                    onChangeText={handleChange('password')}
                                    onBlur={handleBlur('password')}
                                    value={values.password}
                                />
                                <TouchableHighlight underlayColor="transparent" style={styles.pwdicon} onPress={togglePassword}>
                                    <SvgXml xml={passIcon} width="18" height="18" />
                                </TouchableHighlight>
                                {touched.password && errors.password && <Text style={styles.errtxt}>{errors.password}</Text>}
                            </View>
                            <View style={styles.fields}>
                                <CountryCode
                                    data="countrycode"
                                    value={values.countrycode}
                                    setFieldValue={setFieldValue}
                                />
                                <FloatingTextInput
                                    keyboardType="number-pad"
                                    style={styles.mobileinput}
                                    validationStyle={touched.mobileno && errors.mobileno ? styles.invalid : styles.valid}
                                    label="Enter Your Number"
                                    onChangeText={handleChange('mobileno')}
                                    onBlur={handleBlur('mobileno')}
                                    value={values.mobileno}
                                    leftDistance={120}
                                />
                                {touched.mobileno && errors.mobileno && <Text style={styles.errtxt}>{errors.mobileno}</Text>}
                            </View>
                            <View style={styles.fields}>
                                <FloatingTextInput
                                    label="Trade/commercial License No"
                                    onChangeText={handleChange('licenseno')}
                                    onBlur={handleBlur('licenseno')}
                                    value={values.licenseno}
                                />
                            </View>
                            <TouchableHighlight style={styles.termscond} onPress={termsaccept} underlayColor="transparent">
                                <>
                                    <View style={styles.checkouter}>
                                        {acceptTerms ? <SvgXml xml={checkbox} width="9" height="7" /> : null}
                                    </View>
                                    <Text style={styles.termstxt}>Accept Trux Terms and Conditions</Text>
                                </>
                            </TouchableHighlight>
                            <View style={styles.termscond}>
                                <View style={styles.checkouter} />
                                <Text style={styles.termstxt}>Accept to Receive TRUX Notifications via WhatsApp</Text>
                            </View>
                            <TouchableHighlight disabled={!acceptTerms} onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                                <GradientBtn name="Register" disabled={!acceptTerms} />
                            </TouchableHighlight>
                        </>
                    )}
                </Formik>
            </View>
            <Loader isVisible={transLoading} />
        </View>
    );
}

export default AuthHOC(TransporterSingle);
