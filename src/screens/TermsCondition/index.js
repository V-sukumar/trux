import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import RenderHtml from 'react-native-render-html';
import AppHOC from "../../components/AppHOC";
import { getTerms } from "../../store/master/masterSlice";
import Loader from "../../components/Loader";
import { htmlStyle, systemFonts, htmlWidth } from "../PrivacyPolicy/Constants";

function TermsConditions() {
    const dispatch = useDispatch();
    const terms = useSelector((state) => state.master.termsandconditions);
    const isLoading = useSelector((state) => state.master.isLoading);

    useEffect(() => {
        if (terms === "") {
            dispatch(getTerms());
        }
    }, []);

    return (
        <>
            <RenderHtml
                contentWidth={htmlWidth}
                source={{ html: terms }}
                tagsStyles={htmlStyle}
                systemFonts={systemFonts}
            />
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(TermsConditions, "Terms and Conditions");
