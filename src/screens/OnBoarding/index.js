import React from "react";
import { View, TouchableHighlight, Text, ImageBackground } from "react-native";
import Animated, { useAnimatedRef } from 'react-native-reanimated';
import GradientBtn from "../../components/GradientBtn";
import { styles } from "./style";
import { ScreenWidth } from "../../components/Constants";
import * as RootNavigation from "../../components/RootNavigation";

const sliders = [
    { img: require("../../img/onboard/1.png"), head: "Unlock New", subhead: "opportunities", para: "Efficient journey management, transparent transactions, and real-time notifications." },
    { img: require("../../img/onboard/2.png"), head: "Go beyond", subhead: "boundaries", para: "Elevate your reach and expand your transportation network." },
    { img: require("../../img/onboard/3.png"), head: "Effortless truck", subhead: "booking with TRUX!", para: "Select your locations, date, and time, choose your cargo type and pick the perfect truck." },
];

function OnBoarding() {
    const scrollRef = useAnimatedRef();

    function nextscroll(index) {
        scrollRef.current.scrollTo({
            x: ScreenWidth * (index + 1),
            y: 0,
        });
    }

    function navig() {
        RootNavigation.navigate("LoginEmail");
    }

    return (
        <Animated.ScrollView 
            horizontal
            pagingEnabled
            decelerationRate="fast"
            showsHorizontalScrollIndicator={false}
            bounces={false}
            ref={scrollRef}
        >
            {sliders.map((item, i) => (
            <ImageBackground source={item.img} resizeMode="cover" style={styles.bgimage} key={i}>
                <View style={styles.contentview}>
                    <Text style={styles.heading}>{item.head}</Text>
                    <Text style={styles.heading}>{item.subhead}</Text>
                    <Text style={styles.paragraph}>{item.para}</Text>
                </View>                
                <View style={styles.btnview}>
                    {i === 2 ?
                    <TouchableHighlight underlayColor="transparent" style={styles.nextbtn} onPress={navig}>
                        <GradientBtn name="Get Started" />
                    </TouchableHighlight>
                    :
                    <TouchableHighlight underlayColor="transparent" style={styles.nextbtn} onPress={() => nextscroll(i)}>
                        <GradientBtn name="Next" />
                    </TouchableHighlight>}
                </View>                
            </ImageBackground>
            ))}
        </Animated.ScrollView>
    );
}

export default OnBoarding;
