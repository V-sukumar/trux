import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    bgimage: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        width: ScreenWidth,
    },
    contentview: {
        width: ScreenWidth * .8,
        marginTop: "15%",
    },
    heading: {
        color: "#fff",
        fontSize: 36,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 54,
    },
    paragraph: {
        color: "#fff",
        fontSize: 16,
        fontFamily: "Poppins-Regular",
        lineHeight: 30,
        marginTop: 10,
    },
    btnview: {
        marginBottom: "10%",
    },
    nextbtn: {
        height: 50,
        width: ScreenWidth * .8,
    },
});
