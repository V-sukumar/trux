import { ScreenWidth, FontScale } from "../../components/Constants";

export const systemFonts = ['Poppins-Regular', 'Poppins-Medium', "Poppins-SemiBold"];

export const htmlWidth = ScreenWidth - 40;

export const htmlStyle = {
    h1: {
        color: '#084D56',
        marginTop: 0,
        fontSize: 18 / FontScale,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 27 / FontScale,
    },
    h2: {
        color: '#151616',
        marginTop: 0,
        fontSize: 16 / FontScale,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 24 / FontScale,
    },
    h3: {
        color: '#084D56',
        marginTop: 0,
        fontSize: 14 / FontScale,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 22 / FontScale,
    },
    p: {
        color: '#151616',
        marginTop: 0,
        fontSize: 12 / FontScale,
        fontFamily: "Poppins-Regular",
        lineHeight: 22 / FontScale,
    },
};
