import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import RenderHtml from 'react-native-render-html';
import AppHOC from "../../components/AppHOC";
import { getPrivacy } from "../../store/master/masterSlice";
import Loader from "../../components/Loader";
import { htmlWidth, htmlStyle, systemFonts } from "./Constants";

function PrivacyPolicy() {
    const dispatch = useDispatch();
    const privacypolicy = useSelector((state) => state.master.privacypolicy);
    const isLoading = useSelector((state) => state.master.isLoading);

    useEffect(() => {
        if (privacypolicy === "") {
            dispatch(getPrivacy());
        }
    }, []);

    return (
        <>
            <RenderHtml
                contentWidth={htmlWidth}
                source={{ html: privacypolicy }}
                tagsStyles={htmlStyle}
                systemFonts={systemFonts}
            />
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(PrivacyPolicy, "Privacy Policy");
