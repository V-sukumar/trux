import React, { useEffect, useState } from "react";
import { Platform, Alert, Image, TouchableHighlight } from "react-native";
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useRoute, useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ReactNativeBlobUtil from 'react-native-blob-util';
import { SvgXml } from 'react-native-svg';
import { ReactNativeZoomableView } from '@openspacelabs/react-native-zoomable-view';
import { API_URL } from "@env";
import Loader from "../../components/Loader";
import { closewhite } from "../../img/AppIcon";
import { styles } from "../PdfViewer/style";

function ImageViewer() {
    const insets = useSafeAreaInsets();
    const route = useRoute();
    const navigation = useNavigation();
    const { url } = route.params;
    const [imgpath, setImgpath] = useState(null);
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        getfile();
    }, []);

    async function getfile() {
        setLoading(true);
        try {
            const token = await AsyncStorage.getItem("@token");
            const response = await ReactNativeBlobUtil.fetch('GET', `${API_URL}${url}`, {
                Authorization: "Bearer " + JSON.parse(token),
            });
            const imgBlob = await response.data;
            if (imgBlob.length > 50) {
                const imgFilePath = `data:image/jpg;base64,${imgBlob}`;
                setImgpath(imgFilePath);
            } else {
                Alert.alert("Alert", "File not found", [{ text: "OK", onPress: () => navigation.goBack() }], { cancelable: false });
            }
            setLoading(false);
        } catch (e) {
            setLoading(false);
        }
    }

    function backnavig() {
        navigation.goBack()
    }

    return (
        <SafeAreaView
            style={[styles.safearea, { marginTop: Platform.OS === "ios" ? -(insets.top) : 0 }]}
        >
            <TouchableHighlight style={styles.closebtn} onPress={backnavig} underlayColor="#094B54">
                <SvgXml xml={closewhite} width="16" height="16" />
            </TouchableHighlight>
            {imgpath !== null &&
                <ReactNativeZoomableView
                    maxZoom={10}
                    minZoom={1}
                >
                    <Image
                        style={styles.imgfile}
                        source={{ uri: imgpath }}
                        resizeMode="contain"
                    />
                </ReactNativeZoomableView>}
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </SafeAreaView>
    );
}

export default ImageViewer;