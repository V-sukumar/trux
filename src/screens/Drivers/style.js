import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    flashlist: {
        padding: 20,
    },
    accordianContainer: {
        backgroundColor: "#22AFBA",
        borderWidth: 1,
        borderColor: "#22AFBA",
        borderRadius: 8,
        overflow: "hidden",
        marginBottom: 20,
    },
    titleOuter: {
        width: "90%",
    },
    iconOuter: {
        width: "10%",
        alignItems: "flex-end"
    },
    titleContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 14,
        alignItems: "flex-start",
    },
    nametxt: {
        color: "#fff",
        fontSize: 18,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 27,
    },
    mobiltxt: {
        color: "#fff",
        fontSize: 14,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 21,
    },
    contentContainer: {
        position: "absolute",
        top: 0,
        width: "100%",
    },
    contentInner: {
        backgroundColor: "#fff",
        paddingHorizontal: 14,
        paddingVertical: 10,
    },
    inputOuter: {
        borderWidth: 1,
        borderColor: "#CACACA",
        borderRadius: 10,
        justifyContent: "center",
        paddingHorizontal: 10,
        marginBottom: 20,
    },
    headingOuter: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 10,
    },
    contentHeading: {
        fontSize: 16,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 10,
    },
    licenseHeading: {
        paddingVertical: 0,
        marginBottom: 10,
    },
    textInput: {
        height: 50,
    },
    addrInput: {
        height: 80,
    },
    inputHeading: {
        fontSize: 8,
        color: "#383C3D",
        fontFamily: "Poppins-Medium",
    },
    inputValue: {
        fontSize: 14,
        color: "#03353B",
        fontFamily: "Poppins-Medium",
    },
    licenseOuter: {
        flexDirection: "row",
        justifyContent: "flex-start",
        paddingVertical: 10,
    },
    infoView: {
        width: "90%",
    },
    iconContainer: {
        width: "10%",
        alignItems: "flex-start",
    },
    infotxt: {
        color: "#353535",
        fontFamily: "Poppins-Medium",
        fontSize: 14,
    },
    clickBtn: {
        height: 20,
    },
    clickTxt: {
        fontSize: 14,
        color: "#01989E",
        fontFamily: "Poppins-SemiBold",
    },
    activeCont: {
        borderRadius: 6,
        borderWidth: 1,
        borderColor: "#CACACA",
        paddingHorizontal: 10,
        paddingVertical: 16,
        width: "47%",
    },
    statustxt: {
        fontSize: 12,
        fontFamily: "Poppins-SemiBold",
        color: "#084E56",
    },
    statusactive: {
        fontSize: 16,
        fontFamily: "Poppins-SemiBold",
    },
    activetxt: {
        fontSize: 16,
        color: "#01989E",
        fontFamily: "Poppins-SemiBold",
    },
    inactivetxt: {
        color: "#F54646",
    },
    licenseValidity: {
        color: "#353535",
    },
    statusConatiner: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    complainceOuter: {
        flexDirection: "row",
        marginBottom: 20,
    },
    complainceInner: {
        height: 41,
        backgroundColor: "#EDF9FA",
        borderWidth: 1,
        borderColor: "#094B54",
        borderRadius: 10,
        paddingHorizontal: 10,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 10,
    },
    complaincetxt: {
        fontSize: 14,
        lineHeight: 21,
        color: "#383C3D",
        fontFamily: "Poppins-Medium",
    },
    loaderContainer: {
        height: 120,
        justifyContent: "center",
        alignItems: "center",
    },
    nofoundtxt: {
        fontSize: 16,
        color: "#000",
        fontFamily: "Poppins-Regular",
    },
    nodataOuter: {
        height: 200,
        alignItems: "center",
        justifyContent: "flex-end",
    },
    fileInner: {
        width: "90%",
        flexDirection: "row",
        marginVertical: 6,
    },
    fileContainer: {
        borderWidth: 1,
        borderColor: "#CACACA",
        borderRadius: 6,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 6,
    },
    uploadOuter: {
        marginBottom: 20,
    },
});
