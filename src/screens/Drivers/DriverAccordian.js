import React, { useRef } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import Animated, { useSharedValue, useDerivedValue, withTiming, useAnimatedStyle, useAnimatedRef, runOnUI, measure, interpolate, Extrapolation } from "react-native-reanimated";
import { chevron, edit, document } from "../../img/AppIcon";
import { styles } from "./style";

function DriverAccordian(props) {
    const { data } = props;
    const navigation = useNavigation();
    const lastItemId = useRef(data.id);
    const listRef = useAnimatedRef();
    const heightValue = useSharedValue(0);
    const open = useSharedValue(false);
    const progress = useDerivedValue(() =>
        open.value ? withTiming(1) : withTiming(0),
    );
    const iconStyle = useAnimatedStyle(() => ({
        transform: [{ rotate: `${progress.value * -180}deg` }],
    }));
    const heightAnimationStyle = useAnimatedStyle(() => ({
        height: interpolate(progress.value, [0, 1], [0, heightValue.value], Extrapolation.CLAMP),
    }));
    if (data.id !== lastItemId.current) {
        updateItemId();
        open.value = false;
        heightValue.value = 0;
    }

    function navig() {
        navigation.navigate("EditDriver", { id: data.id });
    }

    function clicknavig(fid, ext, file) {
        const url = `drivers/${data?.id}/${file}/${fid}`;
        if (ext === "pdf") {
            navigation.navigate("PdfViewer", { url });
        } else {
            navigation.navigate("ImageViewer", { url });
        }
    }

    function updateItemId() {
        lastItemId.current = data.id;
    }

    function accordioncollapse() {
        updateItemId();
        if (heightValue.value === 0) {
            runOnUI(() => {
                "worklet";
                heightValue.value = measure(listRef).height;
            })();
        }
        open.value = !open.value;
    }

    return (
        <View style={styles.accordianContainer}>
            <TouchableHighlight
                style={styles.titleContainer}
                underlayColor="transparent"
                onPress={accordioncollapse}
            >
                <>
                    <View style={styles.titleOuter}>
                        <Text style={styles.nametxt}>{data?.name}</Text>
                        <Text style={styles.mobiltxt}>{data?.country_code}{data?.contact_no}</Text>
                    </View>
                    <View style={styles.iconOuter}>
                        <Animated.View style={iconStyle}>
                            <SvgXml xml={chevron} width="20" height="20" />
                        </Animated.View>
                    </View>
                </>
            </TouchableHighlight>
            <Animated.View style={heightAnimationStyle}>
                <Animated.View ref={listRef} style={styles.contentContainer}>
                    <View style={styles.contentInner}>
                        <View style={styles.headingOuter}>
                            <Text style={styles.contentHeading}>Driver Details</Text>
                            <TouchableHighlight onPress={navig} underlayColor="transparent">
                                <SvgXml xml={edit} width="20" height="20" />
                            </TouchableHighlight>
                        </View>
                        <View style={[styles.inputOuter, styles.textInput]}>
                            <Text style={styles.inputHeading}>Email Id</Text>
                            <Text style={styles.inputValue}>{data?.email}</Text>
                        </View>
                        <View style={[styles.inputOuter, styles.addrInput]}>
                            <Text style={styles.inputHeading}>Address</Text>
                            <Text style={styles.inputValue}>{data?.address}</Text>
                        </View>
                        <View style={[styles.inputOuter, styles.textInput]}>
                            <Text style={styles.inputHeading}>License no.</Text>
                            <Text style={styles.inputValue}>{data?.license_no}</Text>
                        </View>
                        {data?.license_doc?.length > 0 &&
                            <View style={styles.uploadOuter}>
                                <Text style={styles.contentHeading}>License</Text>
                                <View style={styles.fileContainer}>
                                    <View>
                                        {data?.license_doc?.map((item, i) => (
                                            <View key={i} style={styles.fileInner}>
                                                <View style={styles.iconContainer}>
                                                    <SvgXml xml={document} width="20" height="20" />
                                                </View>
                                                <View style={styles.infoView}>
                                                    <Text style={styles.infotxt}>{`license_doc_${i}.${item.file_ext}`}</Text>
                                                    <TouchableHighlight
                                                        style={styles.clickBtn}
                                                        onPress={() => clicknavig(item.id, item.file_ext, "license_doc")}
                                                        underlayColor="transparent"
                                                    >
                                                        <Text style={styles.clickTxt}>Click to view</Text>
                                                    </TouchableHighlight>
                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                </View>
                            </View>}
                        <View style={styles.statusConatiner}>
                            <View style={styles.activeCont}>
                                <Text style={styles.statustxt}>Status</Text>
                                <Text style={[styles.statusactive, data?.status === "Active" ? styles.activetxt : styles.inactivetxt]}>{data?.status}</Text>
                            </View>
                            <View style={styles.activeCont}>
                                <Text style={styles.statustxt}>License Validity</Text>
                                <Text style={[styles.activetxt, styles.licenseValidity]}>{data?.license_validity}</Text>
                            </View>
                        </View>
                    </View>
                </Animated.View>
            </Animated.View>
        </View>
    );
}

export default DriverAccordian;
