import React, { useEffect } from "react";
import { View, Text, ActivityIndicator } from "react-native";
import { FlashList } from "@shopify/flash-list";
import { useSelector, useDispatch } from "react-redux";
import AppHOCList from "../../components/AppHOC/AppHOCList";
import DriverAccordian from "./DriverAccordian";
import { styles } from "./style";
import { driverlist, fetchdriverInit, fetchdrivermore } from "../../store/driver/driverSlice";
import Loader from "../../components/Loader";

function Drivers() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state) => state.driver.isLoading);
    const loading = useSelector((state) => state.driver.driversloading);
    const page = useSelector((state) => state.driver.driverspage);
    const drivers = useSelector(driverlist);

    useEffect(() => {
        if (drivers.length === 0) {
            dispatch(fetchdriverInit({ page }));
        }
    }, []);

    function renderdriver({ item }) {
        return <DriverAccordian data={item} />
    }

    function loadmore() {
        if (loading && page !== 1) {
            dispatch(fetchdrivermore({ page }));
        }
    }

    function renderfooter() {
        return (
            loading ?
                <View style={styles.loaderContainer}>
                    <Text style={styles.nofoundtxt}>Hang on loading</Text>
                    <ActivityIndicator size="large" color="#094B54" />
                </View>
                :
                null
        );
    }

    function itemseparator() {
        return <View style={styles.accordianitem} />
    }

    return (
        <>
            <View style={styles.container}>
                {drivers.length > 0 ?
                    <FlashList
                        contentContainerStyle={styles.flashlist}
                        data={drivers}
                        renderItem={renderdriver}
                        onEndReached={loadmore}
                        keyExtractor={(_, index) => index.toString()}
                        ItemSeparatorComponent={itemseparator}
                        ListFooterComponent={renderfooter}
                        estimatedItemSize="76"
                    />
                    :
                    <View style={styles.nodataOuter}>
                        <Text style={styles.nofoundtxt}>No data found</Text>
                    </View>
                }
            </View>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOCList(Drivers, "Drivers", "Driver", "AddDriver");
