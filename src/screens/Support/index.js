import React from "react";
import { Text, View, TouchableHighlight, Keyboard } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import GradientBtn from "../../components/GradientBtn";
import AppHOC from "../../components/AppHOC";
import Loader from "../../components/Loader";
import FloatingTextInput from "../../components/FloatingTextInput";
import AddressInput from "../../components/FloatingTextInput/AddressInput";
import { styles } from "../AddDriver/style";
import { supportInit } from "../../store/support/supportSlice";

const validateschema = Yup.object().shape({
    subject: Yup.string().required("Enter subject"),
    message: Yup.string().required("Enter message"),
});

function Support() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state) => state.support.isLoading);

    function handleSupport(values, resetForm) {
        Keyboard.dismiss();
        var formdata = new FormData();
        formdata.append("subject", values.subject);
        formdata.append("message", values.message);
        dispatch(supportInit({ formdata, resetForm }));
    }

    return (
        <View style={{ flex: 1 }}>
            <Formik
                validationSchema={validateschema}
                initialValues={{ subject: "", message: "" }}
                onSubmit={(values, { resetForm }) => handleSupport(values, resetForm)}
            >
                {({ handleChange, handleBlur, touched, errors, handleSubmit, values }) => (
                    <>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.subject && errors.subject ? styles.invalid : styles.valid}
                                label="Subject"
                                onChangeText={handleChange('subject')}
                                onBlur={handleBlur('subject')}
                                value={values.subject}
                            />
                            {touched.subject && errors.subject && <Text style={styles.errtxt}>{errors.subject}</Text>}
                        </View>
                        <View style={styles.fields}>
                            <AddressInput
                                style={styles.addressinput}
                                label="Message"
                                onChangeText={handleChange('message')}
                                onBlur={handleBlur('message')}
                                value={values.message}
                                validationStyle={touched.message && errors.message ? styles.invalid : styles.valid}
                            />
                            {touched.message && errors.message && <Text style={styles.errtxt}>{errors.message}</Text>}
                        </View>
                        <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                            <GradientBtn name="Submit" />
                        </TouchableHighlight>
                    </>
                )}
            </Formik>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </View>
    );
}

export default AppHOC(Support, "Support");
