import React from "react";
import { Text, View, Image } from "react-native";
import { SvgXml } from 'react-native-svg';
import AppHOC from "../../components/AppHOC";
import { styles } from "../TransporterDashboard/style";
import { ongoingtrip, completedtrip, starfull, starhalf } from "../../img/AppIcon";

function DriverDashboard() {
    return (
        <>
            <View style={styles.ratingcontainer}>
                <Text style={styles.ratingtxt}>Your Rating</Text>
                <View style={styles.starcontainer}>
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starfull} width="18" height="18" />
                    <SvgXml style={styles.stars} xml={starhalf} width="18" height="18" />
                </View>
                <Text style={styles.ratingtxt}>{'4.5/5'}</Text>
            </View>
            <Text style={styles.heading}>Trip Details</Text>
            <View style={styles.tripdetails}>
                <View style={styles.totalcontainer}>
                    <Text style={styles.totaltrips}>45</Text>
                    <Text style={styles.totaltriptxt}>Total Trips</Text>
                </View>
                <Image source={require("../../img/truck.png")} />
            </View>
            <View style={styles.tripcontainer}>
                <View style={[styles.triprow, styles.drivertriprow, styles.ongoing]}>
                    <View style={styles.tripicon}>
                        <SvgXml xml={ongoingtrip} width="20" height="20" />
                    </View>
                    <Text style={styles.triptype}>On Going</Text>
                    <Text style={styles.triptype}>Trips</Text>
                    <Text style={styles.tripcount}>17</Text>
                </View>
                <View style={[styles.triprow, styles.drivertriprow, styles.completed]}>
                    <View style={styles.tripicon}>
                        <SvgXml xml={completedtrip} width="20" height="20" />
                    </View>
                    <Text style={styles.triptype}>Completed</Text>
                    <Text style={styles.triptype}>Trips</Text>
                    <Text style={styles.tripcount}>8</Text>
                </View>
            </View>
            <Text style={styles.heading}>Payment Details</Text>
            <View style={[styles.tripdetails, styles.paydetails]}>
                <View style={styles.totalcontainer}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.totaltrips, styles.totalearnings]}>1650</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Total Earnings</Text>
                </View>
            </View>
            <View style={styles.amtcontainer}>
                <View style={[styles.amtinner, styles.amtsettled]}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.amountxt, styles.settledtxt]}>1000</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Amount Settled</Text>
                </View>
                <View style={[styles.amtinner, styles.amtpending]}>
                    <View style={styles.omrcontainer}>
                        <Text style={[styles.amountxt, styles.pendingtxt]}>650</Text>
                        <Text style={[styles.totaltriptxt, styles.omr]}>OMR</Text>
                    </View>
                    <Text style={styles.totaltriptxt}>Amount Pending</Text>
                </View>
            </View>
        </>
    );
}

export default AppHOC(DriverDashboard, "Subramanian");