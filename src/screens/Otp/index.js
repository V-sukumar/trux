import React, { useRef, useEffect, useState, useCallback } from "react";
import { View, TextInput, TouchableHighlight, Text, InteractionManager } from "react-native";
import { useRoute, useFocusEffect } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "./style";
import GradientBtn from "../../components/GradientBtn";
import { otpverify } from "../../store/verifyotp/verifyotpSlice";
import Loader from "../../components/Loader";

function Otp() {
    const dispatch = useDispatch();
    const route = useRoute();
    const email = route.params.email;
    const otpLoading = useSelector((state) => state.verifyotp.isLoading);
    const err = useSelector((state) => state.verifyotp.error);
    const pin1Ref = useRef();
    const pin2Ref = useRef();
    const pin3Ref = useRef();
    const pin4Ref = useRef();
    const pin5Ref = useRef();
    const pin6Ref = useRef();
    const [pin1, setPin1] = useState('');
    const [pin2, setPin2] = useState('');
    const [pin3, setPin3] = useState('');
    const [pin4, setPin4] = useState('');
    const [pin5, setPin5] = useState('');
    const [pin6, setPin6] = useState('');
    const [submitbtn, setSubmitbtn] = useState(true);

    useFocusEffect(
        useCallback(() => {
            const task = InteractionManager.runAfterInteractions(() => {
                pin1Ref.current.focus()
            });      
            return () => task.cancel();
        }, [pin1Ref])
    );

    useEffect(() => {
        if(pin1 !=='' && pin2 !=='' && pin3 !=='' && pin4 !=='' && pin5 !=="" && pin6 !=="") {
            setSubmitbtn(false);
        } else {
            setSubmitbtn(true);
        }
    }, [pin1, pin2, pin3, pin4, pin5, pin6]);

    function setData(pval, setp, pref) {
        setp(pval);
        if(pval !== "") {
            pref.current.focus();
        }
    }

    function clearotp(e, setp, pref) {
        if(e.nativeEvent.key ==='Backspace') {
            setp('');
            pref.current.focus();
        }
    }

    function focusinput(ref) {
        ref.current.focus();
    }

    function handleSubmit() {
        const payload = {
            email,
            otp: pin1+pin2+pin3+pin4+pin5+pin6,
        };
        dispatch(otpverify(payload));
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Enter OTP</Text>
                {err !== '' && <Text style={styles.errortxt}>{err}</Text>}
                <View style={styles.otpbox}>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin1Ref)}>
                        <TextInput
                            ref={pin1Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin1}
                            onKeyPress={(e) => clearotp(e, setPin1, pin1Ref)}
                            onChangeText={(pin1) => setData(pin1, setPin1, pin2Ref)}
                        />
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin2Ref)}>
                        <TextInput
                            ref={pin2Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin2}
                            onKeyPress={(e) => clearotp(e, setPin2, pin1Ref)}
                            onChangeText={(pin2) => setData(pin2, setPin2, pin3Ref)}
                        />
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin3Ref)}>
                        <TextInput
                            ref={pin3Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin3}
                            onKeyPress={(e) => clearotp(e, setPin3, pin2Ref)}
                            onChangeText={(pin3) => setData(pin3, setPin3, pin4Ref)}
                        />
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin4Ref)}>
                        <TextInput
                            ref={pin4Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin4}
                            onKeyPress={(e) => clearotp(e, setPin4, pin3Ref)}
                            onChangeText={(pin4) => setData(pin4, setPin4, pin5Ref)}
                        />
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin5Ref)}>
                        <TextInput
                            ref={pin5Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin5}
                            onKeyPress={(e) => clearotp(e, setPin5, pin4Ref)}
                            onChangeText={(pin5) => setData(pin5, setPin5, pin6Ref)}
                        />
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor="#EDEDED" style={styles.inputouter} onPress={() => focusinput(pin6Ref)}>
                        <TextInput
                            ref={pin6Ref}
                            keyboardType='number-pad' 
                            maxLength={1} 
                            style={styles.otpinput}
                            value={pin6}
                            onKeyPress={(e) => clearotp(e, setPin6, pin5Ref)}
                            onChangeText={(pin6) => setData(pin6, setPin6, pin6Ref)}
                        />
                    </TouchableHighlight>
                </View>
                <View style={styles.didntotp}>
                    <Text style={styles.didntotptxt}>Didn't Receive OTP Resend Code</Text>
                </View>                
                <TouchableHighlight disabled={submitbtn} underlayColor="transparent" onPress={handleSubmit} style={styles.gradientbtn}>
                    <GradientBtn name="Submit" />
                </TouchableHighlight>
            </View>
            <Loader isVisible={otpLoading} />
        </View>
    );
}

export default AuthHOC(Otp);
