import { StyleSheet } from 'react-native';
import { ScreenWidth } from "../../components/Constants";

export const styles = StyleSheet.create({
    viewcont: {
        backgroundColor: "#fff",
        alignItems: "center",
        borderTopLeftRadius: 30,
        borderTopEndRadius: 30,
        width: ScreenWidth,
        paddingBottom: 30,
    },
    formcont: {
        width: (ScreenWidth - 40),
    },
    heading: {
        marginTop: 30,
        fontSize: 24,
        fontFamily: "Poppins-SemiBold",
        color: "#084D56",
        marginBottom: 20,
    },
    otpbox:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom: 40,
    },
    inputouter: {
        borderColor: "#094B54",
        borderWidth: 1,        
        backgroundColor: "#EDEDED",
        borderRadius: 10,
        width: 51,
        height: 56,
        alignItems: "center",
        justifyContent: "center",
    },
    otpinput: {
        width: 20,
        height: 56,
        color: '#000',
        fontSize: 18,
        fontFamily: "Poppins-Regular",
    },
    didntotp: {
        alignItems: "center",
        marginBottom: 40,
    },
    didntotptxt: {
        color: "#053A41",
        fontSize: 12,
        fontFamily: "Poppins-SemiBold",
    },
    thanktxt: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 12,
        color: "#151616",
        textAlign: "center",
    },
    lottiecontainer: {
        alignItems: "center",
        justifyContent: "center",
    },
    lottiecont: {
        height: 180,
        alignItems: "center",        
    },
    lottieview: {
        height: 180,
        width: 180,
    },
    contbtn: {
        marginTop: 30,
        height: 50,
    },
    errortxt: {
        color: '#ff0000',
        fontSize: 16,
        textAlign: 'center',
        marginBottom: 6,
        fontFamily: "Poppins-Regular",
    },
    gradientbtn: {
        height: 50,
    },
});
