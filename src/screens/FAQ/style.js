import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    accordianContainer: {
        backgroundColor: "#fff",
        borderWidth: 1,
        borderColor: "#22AFBA",
        borderRadius: 3,
        overflow: "hidden",
        marginBottom: 20,
    },
    titleContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 14,
        alignItems: "flex-start",
    },
    nametxt: {
        color: "#084D56",
        fontSize: 14,
        fontFamily: "Poppins-SemiBold",
        lineHeight: 21,
    },
    contentContainer: {
        position: "absolute",
        top: 0,
        width: "100%",
        borderTopColor: "#22AFBA",
        borderTopWidth: 1,
    },
    contentInner: {
        backgroundColor: "#fff",
        paddingHorizontal: 14,
        paddingVertical: 10,
    },
    inputValue: {
        fontSize: 12,
        lineHeight: 22,
        color: "#151616",
        fontFamily: "Poppins-Regular",
    },
});
