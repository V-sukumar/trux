import React, { useEffect } from "react";
import { View, Text } from "react-native";
import { FlashList } from "@shopify/flash-list";
import { useSelector, useDispatch } from "react-redux";
import AppHOCList from "../../components/AppHOC/AppHOCList";
import { getFaq } from "../../store/master/masterSlice";
import Loader from "../../components/Loader";
import { styles } from "../Drivers/style";
import FaqAccordian from "./FaqAccordion";

function FAQ() {
    const dispatch = useDispatch();
    const faqs = useSelector((state) => state.master.faqs);
    const isLoading = useSelector((state) => state.master.isLoading);

    useEffect(() => {
        if (faqs.length === 0) {
            dispatch(getFaq());
        }
    }, []);

    function renderfaq({ item }) {
        return <FaqAccordian data={item} />
    }

    function itemseparator() {
        return <View style={styles.accordianitem} />
    }

    return (
        <>
            <View style={styles.container}>
                {faqs.length > 0 ?
                    <FlashList
                        contentContainerStyle={styles.flashlist}
                        data={faqs}
                        renderItem={renderfaq}
                        keyExtractor={(_, index) => index.toString()}
                        ItemSeparatorComponent={itemseparator}
                        estimatedItemSize="76"
                    />
                    :
                    <View style={styles.nodataOuter}>
                        <Text style={styles.nofoundtxt}>No data found</Text>
                    </View>
                }
            </View>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOCList(FAQ, "FAQ");
