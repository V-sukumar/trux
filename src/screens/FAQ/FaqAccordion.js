import React, { useRef } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import Animated, { useSharedValue, useDerivedValue, withTiming, useAnimatedStyle, useAnimatedRef, runOnUI, measure, interpolate, Extrapolation } from "react-native-reanimated";
import { chevronfaq } from "../../img/AppIcon";
import { styles } from "../Drivers/style";
import { styles as fstyles } from "./style";

function FaqAccordian(props) {
    const { data } = props;
    const navigation = useNavigation();
    const lastItemId = useRef(data.id);
    const listRef = useAnimatedRef();
    const heightValue = useSharedValue(0);
    const open = useSharedValue(false);
    const progress = useDerivedValue(() =>
        open.value ? withTiming(1) : withTiming(0),
    );
    const iconStyle = useAnimatedStyle(() => ({
        transform: [{ rotate: `${progress.value * -180}deg` }],
    }));
    const heightAnimationStyle = useAnimatedStyle(() => ({
        height: interpolate(progress.value, [0, 1], [0, heightValue.value], Extrapolation.CLAMP),
    }));
    if (data.id !== lastItemId.current) {
        updateItemId();
        open.value = false;
        heightValue.value = 0;
    }

    function updateItemId() {
        lastItemId.current = data.id;
    }

    function accordioncollapse() {
        updateItemId();
        if (heightValue.value === 0) {
            runOnUI(() => {
                "worklet";
                heightValue.value = measure(listRef).height;
            })();
        }
        open.value = !open.value;
    }

    return (
        <View style={fstyles.accordianContainer}>
            <TouchableHighlight
                style={fstyles.titleContainer}
                underlayColor="transparent"
                onPress={accordioncollapse}
            >
                <>
                    <View style={styles.titleOuter}>
                        <Text style={fstyles.nametxt}>{data?.question}</Text>
                    </View>
                    <View style={styles.iconOuter}>
                        <Animated.View style={iconStyle}>
                            <SvgXml xml={chevronfaq} width="20" height="20" />
                        </Animated.View>
                    </View>
                </>
            </TouchableHighlight>
            <Animated.View style={heightAnimationStyle}>
                <Animated.View ref={listRef} style={fstyles.contentContainer}>
                    <View style={fstyles.contentInner}>
                        <Text style={fstyles.inputValue}>{data?.answer}</Text>
                    </View>
                </Animated.View>
            </Animated.View>
        </View>
    );
}

export default FaqAccordian;
