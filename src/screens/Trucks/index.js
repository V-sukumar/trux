import React, { useEffect } from "react";
import { View, Text, ActivityIndicator } from "react-native";
import { FlashList } from "@shopify/flash-list";
import { useSelector, useDispatch } from "react-redux";
import AppHOCList from "../../components/AppHOC/AppHOCList";
import TruckAccordian from "./TruckAccordian";
import { styles } from "../Drivers/style";
import { trucklist, fetchtruckInit, fetchtruckmore } from "../../store/truck/truckSlice";
import Loader from "../../components/Loader";

function Trucks() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state) => state.truck.isLoading);
    const loading = useSelector((state) => state.truck.trucksloading);
    const page = useSelector((state) => state.truck.truckspage);
    const trucks = useSelector(trucklist);

    useEffect(() => {
        if (trucks.length === 0) {
            dispatch(fetchtruckInit({ page }));
        }
    }, []);

    function rendertruck({ item }) {
        return <TruckAccordian data={item} />
    }

    function loadmore() {
        if (loading && page !== 1) {
            dispatch(fetchtruckmore({ page }));
        }
    }

    function renderfooter() {
        return (
            loading ?
                <View style={styles.loaderContainer}>
                    <Text style={styles.nofoundtxt}>Hang on loading</Text>
                    <ActivityIndicator size="large" color="#094B54" />
                </View>
                :
                null
        );
    }

    function itemseparator() {
        return <View style={styles.accordianitem} />
    }

    return (
        <>
            <View style={styles.container}>
                {trucks.length > 0 ?
                    <FlashList
                        contentContainerStyle={styles.flashlist}
                        data={trucks}
                        renderItem={rendertruck}
                        onEndReached={loadmore}
                        keyExtractor={(_, index) => index.toString()}
                        ItemSeparatorComponent={itemseparator}
                        ListFooterComponent={renderfooter}
                        estimatedItemSize="76"
                    />
                    :
                    <View style={styles.nodataOuter}>
                        <Text style={styles.nofoundtxt}>No data found</Text>
                    </View>
                }
            </View>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOCList(Trucks, "Trucks", "Truck", "AddTruck");
