import React, { useRef } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { SvgXml } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import Animated, { useSharedValue, useDerivedValue, withTiming, useAnimatedStyle, useAnimatedRef, runOnUI, measure, interpolate, Extrapolation } from "react-native-reanimated";
import { chevron, edit, document } from "../../img/AppIcon";
import { styles } from "../Drivers/style";

function TruckAccordian(props) {    
    const { data } = props;
    const navigation = useNavigation();
    const lastItemId = useRef(data.id);
    const listRef = useAnimatedRef();
    const heightValue = useSharedValue(0);
    const open = useSharedValue(false);
    const progress = useDerivedValue(() =>
        open.value ? withTiming(1) : withTiming(0),
    );
    const iconStyle = useAnimatedStyle(() => ({
        transform: [{ rotate: `${progress.value * -180}deg` }],
    }));
    const heightAnimationStyle = useAnimatedStyle(() => ({
        height: interpolate(progress.value, [0, 1], [0, heightValue.value], Extrapolation.CLAMP),
    }));
    if (data.id !== lastItemId.current) {
        updateItemId();
        open.value = false;
        heightValue.value = 0;
    }

    function navig() {
        navigation.navigate("EditTruck", { id: data.id });
    }

    function clicknavig(fid, ext, file) {
        const url = `trucks/${data?.id}/${file}/${fid}`;
        if (ext === "pdf") {
            navigation.navigate("PdfViewer", { url });
        } else {
            navigation.navigate("ImageViewer", { url });
        }
    }

    function updateItemId() {
        lastItemId.current = data.id;
    }

    function accordioncollapse() {
        updateItemId();
        if (heightValue.value === 0) {
            runOnUI(() => {
                "worklet";
                heightValue.value = measure(listRef).height;
            })();
        }
        open.value = !open.value;
    }

    return (
        <View style={styles.accordianContainer}>
            <TouchableHighlight
                style={styles.titleContainer}
                underlayColor="transparent"
                onPress={accordioncollapse}
            >
                <>
                    <View style={styles.titleOuter}>
                        <Text style={styles.nametxt}>{data?.truck_no}</Text>
                        <Text style={styles.mobiltxt}>{data?.truck_type}</Text>
                    </View>
                    <View style={styles.iconOuter}>
                        <Animated.View style={iconStyle}>
                            <SvgXml xml={chevron} width="20" height="20" />
                        </Animated.View>
                    </View>
                </>
            </TouchableHighlight>
            <Animated.View style={heightAnimationStyle}>
                <Animated.View ref={listRef} style={styles.contentContainer}>
                    <View style={styles.contentInner}>
                        <View style={styles.headingOuter}>
                            <Text style={styles.contentHeading}>Truck Details</Text>
                            <TouchableHighlight onPress={navig} underlayColor="transparent">
                                <SvgXml xml={edit} width="20" height="20" />
                            </TouchableHighlight>
                        </View>
                        {data?.reg_card?.length > 0 &&
                        <View style={styles.uploadOuter}>
                            <Text style={styles.contentHeading}>Registration Card</Text>
                            <View style={styles.fileContainer}>
                                <View>
                                    {data?.reg_card?.map((item, i) => (
                                    <View key={i} style={styles.fileInner}>
                                        <View style={styles.iconContainer}>
                                            <SvgXml xml={document} width="20" height="20" />
                                        </View>
                                        <View style={styles.infoView}>
                                            <Text style={styles.infotxt}>{`reg_card_${i}.${item.file_ext}`}</Text>
                                            <TouchableHighlight
                                                style={styles.clickBtn}
                                                onPress={() => clicknavig(item.id, item.file_ext, "reg_card")}
                                                underlayColor="transparent"
                                            >
                                                <Text style={styles.clickTxt}>Click to view</Text>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                    ))}
                                </View>
                            </View>
                        </View>}
                        {data?.insurance?.length > 0 &&
                        <View style={styles.uploadOuter}>
                            <Text style={styles.contentHeading}>Insurance Card</Text>
                            <View style={styles.fileContainer}>
                                <View>
                                    {data?.insurance?.map((item, i) => (
                                    <View key={i} style={styles.fileInner}>
                                        <View style={styles.iconContainer}>
                                            <SvgXml xml={document} width="20" height="20" />
                                        </View>
                                        <View style={styles.infoView}>
                                            <Text style={styles.infotxt}>{`insurance_${i}.${item.file_ext}`}</Text>
                                            <TouchableHighlight
                                                style={styles.clickBtn}
                                                onPress={() => clicknavig(item.id, item.file_ext, "insurance")}
                                                underlayColor="transparent"
                                            >
                                                <Text style={styles.clickTxt}>Click to view</Text>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                    ))}
                                </View>
                            </View>
                        </View>}
                        <View style={styles.statusConatiner}>
                            <View style={styles.activeCont}>
                                <Text style={styles.statustxt}>Registration Validity</Text>
                                <Text style={styles.activetxt}>{data?.reg_expire_date}</Text>
                            </View>
                            <View style={styles.activeCont}>
                                <Text style={styles.statustxt}>Insurance Validity</Text>
                                <Text style={[styles.activetxt, styles.licenseValidity]}>{data?.insurance_validity}</Text>
                            </View>
                        </View>
                        {data?.cargo_insurance?.length > 0 &&
                        <View style={styles.uploadOuter}>
                            <Text style={styles.contentHeading}>Cargo Insurance</Text>
                            <View style={styles.fileContainer}>
                                <View>
                                    {data?.cargo_insurance?.map((item, i) => (
                                    <View key={i} style={styles.fileInner}>
                                        <View style={styles.iconContainer}>
                                            <SvgXml xml={document} width="20" height="20" />
                                        </View>
                                        <View style={styles.infoView}>
                                            <Text style={styles.infotxt}>{`cargo_insurance_${i}.${item.file_ext}`}</Text>
                                            <TouchableHighlight
                                                style={styles.clickBtn}
                                                onPress={() => clicknavig(item.id, item.file_ext, "cargo_insurance")}
                                                underlayColor="transparent"
                                            >
                                                <Text style={styles.clickTxt}>Click to view</Text>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                    ))}
                                </View>
                            </View>
                        </View>}
                        {data?.truck_photo?.length > 0 &&
                        <View style={styles.uploadOuter}>
                            <Text style={styles.contentHeading}>Truck Photo</Text>
                            <View style={styles.fileContainer}>
                                <View>
                                    {data?.truck_photo?.map((item, i) => (
                                    <View key={i} style={styles.fileInner}>
                                        <View style={styles.iconContainer}>
                                            <SvgXml xml={document} width="20" height="20" />
                                        </View>
                                        <View style={styles.infoView}>
                                            <Text style={styles.infotxt}>{`truck_photo_${i}.${item.file_ext}`}</Text>
                                            <TouchableHighlight
                                                style={styles.clickBtn}
                                                onPress={() => clicknavig(item.id, item.file_ext, "truck_photo")}
                                                underlayColor="transparent"
                                            >
                                                <Text style={styles.clickTxt}>Click to view</Text>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                    ))}
                                </View>
                            </View>
                        </View>}
                        <View style={[styles.headingOuter, styles.licenseHeading]}>
                            <Text style={styles.contentHeading}>Compliances</Text>
                        </View>
                        <View style={styles.complainceOuter}>                            
                            {JSON.parse(data?.compilances).map((item, i) => (
                                <View style={styles.complainceInner} key={i}>
                                    <Text style={styles.complaincetxt}>{item}</Text>
                                </View>
                            ))}
                        </View>
                    </View>
                </Animated.View>
            </Animated.View>
        </View>
    );
}

export default TruckAccordian;
