import React, { useState } from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useNavigation } from '@react-navigation/native';
import { SvgXml } from 'react-native-svg';
import GradientBtn from "../../components/GradientBtn";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../LoginEmail/style";
import { eyeoff } from "../../img/AuthIcon";
import FloatingTextInput from "../../components/FloatingTextInput";
import CountryCode from "../../components/CountryCode";

const validateschema = Yup.object().shape({
    mobileno: Yup.string().matches(/^[0-9]{10}$/,"Enter 10 digit mobile no.").required('Enter Mobile no.'),
    password: Yup.string().required("Enter Password"),
});

function LoginMobile() {
    const navigation = useNavigation();
    const [secureText, setSecuretext] = useState(true);

    function togglePassword() {
        setSecuretext(!secureText);
    }

    function handlelogin(values) {
        console.log(values)
    }

    function navig(page) {
        navigation.navigate(page);
    }

    return (
        <View style={styles.viewcont}>
            <View style={styles.formcont}>
                <Text style={styles.heading}>Login</Text>
                <Formik
                    validationSchema={validateschema}
                    initialValues={{ mobileno: "", password: "", country_code: "961" }}
                    onSubmit={values => handlelogin(values)}
                >
                    {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                    <>
                    <View style={styles.fields}>
                        <CountryCode
                            data="country_code"
                            value={values.country_code}
                            setFieldValue={setFieldValue}
                        />
                        <FloatingTextInput
                            keyboardType="number-pad"
                            style={styles.mobileinput}
                            validationStyle={touched.mobileno && errors.mobileno ? styles.invalid : styles.valid}
                            label="Enter Your Mobile no."
                            onChangeText={handleChange('mobileno')}
                            onBlur={handleBlur('mobileno')}
                            value={values.mobileno}
                            leftDistance={120}
                        />
                        {touched.mobileno && errors.mobileno && <Text style={styles.errtxt}>{errors.mobileno}</Text>}
                    </View>
                    <View style={styles.forgotpwdcont}>
                        <TouchableHighlight underlayColor="transparent" onPress={() => navig("LoginEmail")}>
                            <Text style={styles.forgotpwd}>Use Email id</Text>
                        </TouchableHighlight>                        
                    </View>
                    <View style={styles.fields}>
                        <FloatingTextInput
                            secureTextEntry={secureText}
                            style={styles.pwdinput}
                            validationStyle={touched.password && errors.password ? styles.invalid : styles.valid}
                            label="Password"
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                        />
                        <TouchableHighlight underlayColor="transparent" style={styles.pwdicon} onPress={togglePassword}>
                            <SvgXml xml={eyeoff} width="18" height="18" />
                        </TouchableHighlight>
                        {touched.password && errors.password && <Text style={styles.errtxt}>{errors.password}</Text>}
                    </View>
                    <View style={styles.forgotpwdcont}>
                        <TouchableHighlight underlayColor="transparent">
                            <Text style={styles.forgotpwd}>Forgot password?</Text>
                        </TouchableHighlight>
                    </View>
                    <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.gradientbtn}>
                        <GradientBtn name="Login" />
                    </TouchableHighlight>
                    </>
                    )}
                </Formik>
                <View style={styles.newreg}>
                    <View style={styles.newregheading}>
                        <View style={styles.bgline}/>
                        <Text style={styles.newregtxt}>New User Register Here!</Text>
                    </View>
                    <View style={styles.btncont}>
                        <TouchableHighlight style={styles.newregbtn} underlayColor="transparent" onPress={() =>navig("SigninCustomer")}>
                            <Text style={styles.newregbtntxt}>Customer</Text>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.newregbtn} underlayColor="transparent" onPress={() =>navig("SigninTransporter")}>
                            <Text style={styles.newregbtntxt}>Transporter</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        </View>        
    );
}

export default AuthHOC(LoginMobile);
