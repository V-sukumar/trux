import React, { useState } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import FloatingTextInput from "../../components/FloatingTextInput";
import CountryCode from "../../components/CountryCode";
import GradientBtn from "../../components/GradientBtn";
import { styles } from "../AddDriver/style";
import { styles as dstyles } from "../Drivers/style";
import AppHOC from "../../components/AppHOC";
import { updateInit } from "../../store/customerprofile/customerprofileSlice";
import Loader from "../../components/Loader";

const validateschema = Yup.object().shape({
    name: Yup.string().required("Enter Your Name"),
    mobileno: Yup.string().matches(/^\d{7,10}$/, 'Must be between 7 and 10 digit').required('Enter Mobile no.'),
});

function CustomerIndividualProfile() {
    const dispatch = useDispatch();
    const userinfo = useSelector((state) => state.auth.userInfo);
    const custinfo = useSelector((state) => state.customerprofile.profileInfo);
    const isLoading = useSelector((state) => state.customerprofile.isLoading);

    function handleupdateProfile(values) {
        var formdata = new FormData();
        formdata.append("name", values.name);
        formdata.append("country_code", values.country_code);
        formdata.append("contact_no", values.mobileno);
        formdata.append("email", custinfo?.email);
        const payload = {
            ref_id: userinfo?.ref_id,
            newdata: {
                name: values.name,
                email: custinfo?.email,
                contact_no: values.mobileno,
                country_code: values.country_code,
            },
            formdata,
        };
        dispatch(updateInit(payload));
    }

    return (
        <>
            <Formik
                validationSchema={validateschema}
                initialValues={{ name: custinfo?.name, mobileno: custinfo?.contact_no.toString(), country_code: custinfo?.country_code }}
                onSubmit={values => handleupdateProfile(values)}
            >
                {({ handleChange, handleBlur, touched, errors, handleSubmit, values, setFieldValue }) => (
                    <>
                        <View style={styles.fields}>
                            <FloatingTextInput
                                validationStyle={touched.name && errors.name ? styles.invalid : styles.valid}
                                label="Your Name"
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                                value={values.name}
                            />
                            {touched.name && errors.name && <Text style={styles.errtxt}>{errors.name}</Text>}
                        </View>
                        <View style={[dstyles.inputOuter, dstyles.textInput]}>
                            <Text style={dstyles.inputHeading}>Email Id</Text>
                            <Text style={dstyles.inputValue}>{custinfo?.email}</Text>
                        </View>
                        <View style={styles.fields}>
                            <CountryCode
                                data="code"
                                value={values.country_code}
                                setFieldValue={setFieldValue}
                            />
                            <FloatingTextInput
                                keyboardType="number-pad"
                                style={styles.mobileinput}
                                validationStyle={touched.mobileno && errors.mobileno ? styles.invalid : styles.valid}
                                label="Enter Your Mobile no."
                                onChangeText={handleChange('mobileno')}
                                onBlur={handleBlur('mobileno')}
                                value={values.mobileno}
                                leftDistance={120}
                            />
                            {touched.mobileno && errors.mobileno && <Text style={styles.errtxt}>{errors.mobileno}</Text>}
                        </View>
                        <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                            <GradientBtn name="Submit" />
                        </TouchableHighlight>
                    </>
                )}
            </Formik>
            <Loader isVisible={isLoading} backdropColor="transparent" />
        </>
    );
}

export default AppHOC(CustomerIndividualProfile, "My Profile");
