import React from "react";
import { View, TouchableHighlight, Text } from "react-native";
import { Formik } from 'formik';
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import GradientBtn from "../../components/GradientBtn";
import AuthHOC from "../../components/AuthHOC";
import { styles } from "../CustomerIndividual/style";
import { forgotInit } from "../../store/forgotpassword/forgotpasswordSlice";
import FloatingTextInput from "../../components/FloatingTextInput";
import Loader from "../../components/Loader";

const validateschema = Yup.object().shape({
    email: Yup.string().required('Enter Email').email('Invalid email'),
});

function ForgotPassword() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state) => state.forgotpassword.isLoading);
    const error = useSelector((state) => state.forgotpassword.error);

    function handleforgotpwd(values) {
        const payload = {
            email: values.email,
            forgot_password: 1,
        };
        dispatch(forgotInit(payload));
    }

    return (
        <>
            <View style={[styles.viewcont, styles.forgotpwdcont]}>
                <View style={styles.formcont}>
                    <Text style={styles.heading}>Forgot Password</Text>
                    {error !== "" && <Text style={styles.errortxt}>{error}</Text>}
                    <Formik
                        validationSchema={validateschema}
                        initialValues={{ email: "" }}
                        onSubmit={values => handleforgotpwd(values)}
                    >
                        {({ handleChange, handleBlur, touched, errors, handleSubmit, values }) => (
                            <>
                                <View style={styles.fields}>
                                    <FloatingTextInput
                                        keyboardType="email-address"
                                        validationStyle={touched.email && errors.email ? styles.invalid : styles.valid}
                                        label="Enter Your Email"
                                        onChangeText={handleChange('email')}
                                        onBlur={handleBlur('email')}
                                        value={values.email}
                                    />
                                    {touched.email && errors.email && <Text style={styles.errtxt}>{errors.email}</Text>}
                                </View>
                                <TouchableHighlight onPress={handleSubmit} underlayColor="transparent" style={styles.regbtn}>
                                    <GradientBtn name="Submit" />
                                </TouchableHighlight>
                            </>
                        )}
                    </Formik>
                </View>
            </View>
            <Loader isVisible={isLoading} />
        </>
    );
}

export default AuthHOC(ForgotPassword);
