import React from 'react';
import { StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import HeaderLeft from '../components/Header/HeaderLeft';
import HeaderRight from '../components/Header/HeaderRight';
import TransporterDashboard from '../screens/TransporterDashboard';
import ProfileStatus from '../screens/ProfileStatus';
import Drivers from '../screens/Drivers';
import AddDriver from '../screens/AddDriver';
import EditDriver from '../screens/EditDriver';
import Trucks from '../screens/Trucks';
import AddTruck from '../screens/AddTruck';
import EditTruck from '../screens/EditTruck';
import DriverDashboard from '../screens/DriverDashboard';
import TruckBooking from '../screens/TruckBooking';
import CustomerIndividualProfile from '../screens/CustomerIndividualProfile';
import ChangePassword from '../screens/ChangePassword';
import TransporterProfile from '../screens/TransporterProfile';
import CustomerEntityProfile from '../screens/CustomerEntityProfile';
import PdfViewer from '../screens/PdfViewer';
import ImageViewer from '../screens/ImageViewer';
import PrivacyPolicy from '../screens/PrivacyPolicy';
import TermsCondition from '../screens/TermsCondition';
import Support from '../screens/Support';
import FAQ from '../screens/FAQ';

const forFade = ({ current }) => ({
    cardStyle: {
        opacity: current.progress,
    },
});

const styleheader = {
    height: 56,
};

const AppStack = createStackNavigator();
function Appstack() {
    const userinfo = useSelector((state) => state.auth.userInfo);

    function getInitroute() {
        if (userinfo?.is_admin_verified === false || userinfo?.is_trade_doc_available === false) {
            return "ProfileStatus";
        }
        else {
            switch (userinfo?.user_type) {
                case "transporter":
                    return "TransporterDashboard";
                case "driver":
                    return "DriverDashboard";
                case "customer":
                    return "TruckBooking";
                default:
                    return "ProfileStatus";
            }
        }
    }
    const initRoute = getInitroute();

    return (
        <AppStack.Navigator
            initialRouteName={initRoute}
            screenOptions={{
                cardStyleInterpolator: forFade,
                headerBackground: () => <LinearGradient colors={['#094B54', '#087077']} style={styles.header} />,
                headerStyle: styleheader,
                headerLeft: () => <HeaderLeft />,
                headerRight: () => <HeaderRight />,
                headerStatusBarHeight: 0,
                title: null,
            }}
        >
            <AppStack.Screen name="ProfileStatus" component={ProfileStatus} />
            <AppStack.Screen name="TransporterDashboard" component={TransporterDashboard} />
            <AppStack.Screen name="DriverDashboard" component={DriverDashboard} />
            <AppStack.Screen name="Drivers" component={Drivers} />
            <AppStack.Screen name="AddDriver" component={AddDriver} />
            <AppStack.Screen name="EditDriver" component={EditDriver} />
            <AppStack.Screen name="Trucks" component={Trucks} />
            <AppStack.Screen name="AddTruck" component={AddTruck} />
            <AppStack.Screen name="EditTruck" component={EditTruck} />
            <AppStack.Screen name="TruckBooking" component={TruckBooking} />
            <AppStack.Screen name="CustomerIndividualProfile" component={CustomerIndividualProfile} />
            <AppStack.Screen name="ChangePassword" component={ChangePassword} />
            <AppStack.Screen name="TransporterProfile" component={TransporterProfile} />
            <AppStack.Screen name="CustomerEntityProfile" component={CustomerEntityProfile} />
            <AppStack.Screen name="PdfViewer" component={PdfViewer} options={{ headerShown: false }} />
            <AppStack.Screen name="ImageViewer" component={ImageViewer} options={{ headerShown: false }} />
            <AppStack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
            <AppStack.Screen name="TermsCondition" component={TermsCondition} />
            <AppStack.Screen name="Support" component={Support} />
            <AppStack.Screen name="FAQ" component={FAQ} />
        </AppStack.Navigator>
    );
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
    },
});

export default Appstack;
