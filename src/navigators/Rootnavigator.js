import React from 'react';
import { StatusBar, View, StyleSheet } from "react-native";
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useSelector } from 'react-redux';
import Authstack from "./Authstack";
import Appdrawer from './Appdrawer';

function Rootnavigator() {
    const insets = useSafeAreaInsets();
    const userinfo = useSelector((state) => state.auth.userInfo);

    return (
        <>
            <View style={[styles.statusbar, { height: insets.top }]}>
                <StatusBar backgroundColor="#094B54" barStyle="light-content" />
            </View>
            {userinfo === null ? <Authstack /> : <Appdrawer />}
        </>
    );
}

const styles = StyleSheet.create({
    statusbar: {
        backgroundColor: "#094B54",
    },
});

export default Rootnavigator;
