import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Appstack from './Appstack';
import { ScreenWidth } from '../components/Constants';
import DrawerContent from '../components/DrawerContent';

const AppDrawer = createDrawerNavigator();
function Appdrawer() {
    return (
        <AppDrawer.Navigator
            screenOptions={{
                headerShown: false,
                drawerStyle: {
                    backgroundColor: '#fff',
                    width: ScreenWidth * .8,
                },
                overlayColor: 'rgba(0, 0, 0, 0.4)',
            }}
            initialRouteName="Appstack"
            drawerContent={(props) => <DrawerContent {...props} />}
        >
            <AppDrawer.Screen name="Appstack" component={Appstack} />
        </AppDrawer.Navigator>
    );
}

export default Appdrawer;
