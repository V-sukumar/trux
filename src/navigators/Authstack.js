import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import LoginEmail from "../screens/LoginEmail";
import LoginMobile from '../screens/LoginMobile';
import SigninCustomer from '../screens/SigninCustomer';
import SigninTransporter from '../screens/SigninTransporter';
import CustomerIndividual from '../screens/CustomerIndividual';
import CustomerEntity from '../screens/CustomerEntity';
import TransporterSingle from '../screens/TransporterSingle';
import TransporterMultiple from '../screens/TransporterMultiple';
import Otp from '../screens/Otp';
import Thankyou from '../screens/Thankyou';
import ForgotPassword from '../screens/ForgotPassword';
import ChangePassword from '../screens/ChangePassword';
import OnBoarding from '../screens/OnBoarding';

const forFade = ({ current }) => ({
    cardStyle: {
      opacity: current.progress,
    },
});

const AuthStack = createStackNavigator();
function Authstack() {
    return (
        <AuthStack.Navigator
            initialRouteName="OnBoarding"
            screenOptions={{
                headerShown: false,
                cardStyleInterpolator: forFade,
            }}
        >
            <AuthStack.Screen name="OnBoarding" component={OnBoarding} />
            <AuthStack.Screen name="LoginEmail" component={LoginEmail} />
            <AuthStack.Screen name="LoginMobile" component={LoginMobile} />
            <AuthStack.Screen name="SigninCustomer" component={SigninCustomer} />
            <AuthStack.Screen name="SigninTransporter" component={SigninTransporter} />
            <AuthStack.Screen name="CustomerIndividual" component={CustomerIndividual} />
            <AuthStack.Screen name="CustomerEntity" component={CustomerEntity} />
            <AuthStack.Screen name="TransporterSingle" component={TransporterSingle} />
            <AuthStack.Screen name="TransporterMultiple" component={TransporterMultiple} />
            <AuthStack.Screen name="Otp" component={Otp} />
            <AuthStack.Screen name="Thankyou" component={Thankyou} options={{ gestureEnabled: false }} />
            <AuthStack.Screen name="ForgotPassword" component={ForgotPassword} />
            <AuthStack.Screen name="ChangePassword" component={ChangePassword} />
        </AuthStack.Navigator>
    );
}

export default Authstack;
