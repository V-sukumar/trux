import React, { useEffect } from 'react';
import { NativeModules } from 'react-native';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from "react-redux";
import Rootnavigator from './src/navigators/Rootnavigator';
import { navigationRef } from './src/components/RootNavigation';
import { authSuccess } from './src/store/auth/authSlice';
import { NotificationsProvider } from './src/components/Constants';

function App() {
  const dispatch = useDispatch();
  const { AppupdateModule } = NativeModules;

  useEffect(() => {
    checkLogin();
    AppupdateModule.checkForAppUpate();
  }, []);

  async function checkLogin() {
    try {
      let userinfo = await AsyncStorage.getItem('@userinfo');
      console.log("app", userinfo);
      dispatch(authSuccess(JSON.parse(userinfo)));
    } catch (e) {
    }
  }

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <SafeAreaProvider>
        <NavigationContainer ref={navigationRef}>
          <Rootnavigator />
          <NotificationsProvider />
        </NavigationContainer>
      </SafeAreaProvider>
    </GestureHandlerRootView>
  );
}

export default App;
